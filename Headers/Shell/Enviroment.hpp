#ifndef __LUNA_SHELL_ENVIROMENT__
#define __LUNA_SHELL_ENVIROMENT__
#include "Shell/Types.hpp"
#include <unordered_map>
namespace Luna{
    namespace Shell{
		class Enviroment_cl{
			struct Hasher{
                size_t operator()(Value_st* V) const noexcept {
                    return std::hash<std::string>{}(V->String());
                }
            };
            struct Equator{
                constexpr bool operator()(const Value_st* AV,const Value_st* BV) const{
                    return AV->String() == BV->String();
                }
            };
			std::unordered_map<SymbolValue_st*,Value_st*,Hasher,Equator> m_Variables;
            Enviroment_cl* m_Parent;
			public:
                Enviroment_cl(Enviroment_cl* Parent=nullptr);
				void Set(SymbolValue_st* Key,Value_st* Value);
				Value_st* Get(SymbolValue_st* Key);
                Enviroment_cl* Find(SymbolValue_st* Key);
		};
	};
};
#endif