#ifndef __LUNA_FUNCTIONS__
#define __LUNA_FUNCTIONS__
#include <stdfloat>
#include <cmath>
namespace Luna{
    namespace Neural_Networking{
        namespace Functions{
            inline std::float64_t F1_Activate(std::float64_t I,std::float64_t R){
                std::float64_t II=std::tanh(I);
                return II*R;
            }
            inline std::float64_t F1_Derive(std::float64_t I,std::float64_t R){
                std::float64_t II=I/R;
                return 1.0 - II * II;
            }
        };
    };
};
#endif