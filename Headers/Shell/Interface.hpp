#ifndef __LUNA_SHELL_INTERFACE__
#define __LUNA_SHELL_INTERFACE__
#include "Shell/Functions.hpp"
#include "Shell/Enviroment.hpp"
#include "Shell/Types.hpp"
#include "Utilities/Arena_Allocator.hpp"

#include <string>

namespace Luna{
    namespace Shell{
        class Interface_cl{
            Enviroment_cl* RootEnviroment;
            Luna::Utilities::Arena_Allocator_cl Arena_Allocator;
            private:
                Value_st* eval_ast(Value_st* Input,Enviroment_cl* Env);
            private:
                Value_st* READ(std::string Input);
                Value_st* EVAL(Value_st* Input,Enviroment_cl* Env);
                std::string PRINT(Value_st* Input);
            public:
                std::string Repl(std::string Input);
                Interface_cl();
        };
    };
};
#endif