#ifndef __LUNA_UTILITIES_RANDOM__
#define __LUNA_UTILITIES_RANDOM__
#include <stdfloat>
#include <cstdint>
#include <cstddef>
namespace Luna{
    namespace Utilities{
        class Random_cl{
            size_t m_Max{0};
            size_t m_Seed{0};
        public:
            Random_cl(int64_t Seed,int64_t Max);
            size_t Generate_Int();
            std::float64_t Generate_Float();
            size_t Max();
            size_t Max(size_t nMax);
        };
    };
};
#endif