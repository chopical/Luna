#ifndef __LUNA_SHELL_FUNCTIONS__
#define __LUNA_SHELL_FUNCTIONS__
#include "Shell/Types.hpp"
namespace Luna{
    namespace Shell{
		namespace Functions{
			//standard math
			Value_st* F_add(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_subtract(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_multiply(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_divide(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			//comparison
			Value_st* F_greater(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_lesser(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_equivalent(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_and(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_or(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_equivalent_or_greater(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_equivalent_or_lesser(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			//experimental
			Value_st* F_selection(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_sort(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_size(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_recursive_size(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_print(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
			Value_st* F_recombinator(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
		};
	};
};
#endif