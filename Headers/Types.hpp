#ifndef __LUNA_TYPES__
#define __LUNA_TYPES__
#include <stdfloat>
#include <vector>
namespace Luna{
    typedef std::vector<std::float64_t> Vector_nt;
    typedef std::vector<Vector_nt> Matrix_nt;
    struct neuron_nt{std::float64_t &Weight,&Bias,&Value;};
    typedef std::vector<size_t> size_nt;
};
#endif