#include<Networking/Parsers/HTTP.hpp>
#include<iostream>
namespace Luna{
	namespace Networking{
		namespace Parsers{
			namespace HTTP{
				std::deque<Propertie_st> Tokenize(std::string PageSource){
					size_t end_index{0},start_index{0};
					std::deque<Propertie_st> Tokens;
					while(end_index<PageSource.size()){
						if(PageSource[end_index]==':'){
								size_t length=end_index-start_index;
								std::string Tag=PageSource.substr(start_index,length);
								start_index=end_index+1;
								while(++end_index<PageSource.size()&&PageSource[end_index]!='\n'){}
								length=end_index-start_index;
								std::string Value=PageSource.substr(start_index,length);
								start_index=end_index+1;
								Propertie_st Prop{"",""};
								for(size_t i=0;i<Tag.size();i++){if(!(Tag[i]=='\r'||Tag[i]=='\n'))Prop.Tag+=Tag[i];}
								for(size_t i=0;i<Value.size();i++){if(!(Value[i]=='\r'||Value[i]=='\n'))Prop.Value+=Value[i];}
								Tokens.push_back(Prop);
							}
						end_index++;
					}
					return Tokens;
				}
				Header_st Parse(std::deque<Propertie_st> Token){
					Header_st Header;
					size_t space_count{0},oldi{0};
					std::string tmpString{""};
					for(size_t i=0;i<Token[0].Tag.size();i++){
						if(oldi==0&&Token[0].Tag[i]==' '){
							Header.Type=Token[0].Tag.substr(oldi,1+(i-oldi));
							i++;
							oldi=i;
						}else if(Token[0].Tag[i]==' '){
							Header.Directory=Token[0].Tag.substr(oldi,1+(i-oldi));
							break;
						}
					}
					for(size_t i=1;i<Token.size();i++){
						if(Token[i].Tag=="Connection"){
							Header.Connection=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Cache-Control"){
							Header.Cache_Control=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="DNT"){
							Header.DNT=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Upgrade-Insecure-Requests"){
							Header.Upgrade_Insecure_Requests=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="User-Agent"){
							Header.User_Agent=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Accept"){
							Header.Accept=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Sec-Fetch-Site"){
							Header.Sec_Fetch_Site=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Sec-Fetch-Mode"){
							Header.Sec_Fetch_Mode=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Sec-Fetch-User"){
							Header.Sec_Fetch_User=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Sec-Fetch-Dest"){
							Header.Sec_Fetch_Dest=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Accept-Encoding"){
							Header.Accept_Encoding=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Accept-Language"){
							Header.Accept_Language=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else
						if(Token[i].Tag=="Cookie"){
							Header.Cookie=Token[i].Value.substr(1,Token[i].Value.size()-1);
						}else{
							Header.Others.push_back(Token[i]);
						}
					}
					return Header;
				}
				std::string ResponseHTTP(std::string Content,int64_t ResponseCode, std::string Cookie){
					std::string Result="HTTP/1.1 ";
					switch(ResponseCode){
						case 200:{
							Result+="200 OK";
						}break;
						default:{
							Result+=std::to_string(ResponseCode);
						}break;
					}
					Result+="\n";
					Result+=std::string("Server: ")+std::string("Luna");
					Result+=std::string("/")+std::string("0.0.0.0");
					Result+="\n";
					Result+=std::string("Content-Type:" )+std::string("text/html");
					Result+="\n";
					Result+=std::string("Content-Length: ")+std::to_string(Content.size());
					Result+="\n";
					Result+=std::string("Accept-Ranges: ")+std::string("bytes");
					Result+="\n";
					if(Cookie!=".")Result+=std::string("Set-Cookie: ")+Cookie+std::string("\n");
					Result+="\n";
					Result+=Content;
					return Result;
				}
			};
		};
	};
};





//"Content-Length: 15\n"
//"Accept-Ranges: bytes\n"
//"Connection: close\n"
//"\n"
//"sdfkjsdnbfkjbsf";




