#include "Networking/Socket.hpp"
#include "Utilities/Random.hpp"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


namespace Luna{
	namespace Networking{
		//// CLIENT
		SocketClient_st::SocketClient_st(std::string IP,int Port,int Domain,int Type,int Protocol){
			Properties.Domain=Domain;
			Properties.Type=Type;
			Properties.Protocol=Protocol;
			Properties.PortNumber=Port;
			Properties.Descriptor=socket(Properties.Domain,Properties.Type,Properties.Protocol);
			if(Properties.Descriptor==-1){
				perror("Failed to create socket.");
				exit(1);
			}
			Properties.Sockaddr.sin_addr.s_addr = inet_addr(IP.c_str());
			Properties.Sockaddr.sin_family=Properties.Domain;
			Properties.Sockaddr.sin_port = htons(Properties.PortNumber);
			if(connect(Properties.Descriptor,(struct sockaddr *)&Properties.Sockaddr, sizeof(Properties.Sockaddr)) < 0){
				perror("Failed to connect.");
				exit(2);
			}
		}
		SocketClient_st::~SocketClient_st(){
			close(Properties.Descriptor);
		}
		void SocketClient_st::Send(std::string msg){
			if(send(Properties.Descriptor,msg.c_str(),msg.size(),0) < 0){
				perror("Failed to send message.");
				exit(3);
			}
		}
		std::string SocketClient_st::Receive(size_t bufferSize){
			char* replyBuffer = new char[bufferSize+1];
			memset(replyBuffer,0,bufferSize+1);
			if(recv(Properties.Descriptor,replyBuffer,bufferSize,0) < 0){
				perror("Failed to receive data");
				exit(4);
			}
			if(replyBuffer[bufferSize+1] != 0){
				perror("Overflow!");
				exit(5);
			}
			std::string reply(replyBuffer);
			memset(replyBuffer,0,bufferSize+1);
			delete[] replyBuffer;
			return reply;
		}
		//// SERVER
		SocketServer_st::SocketServer_st(std::string IP,int Port,int Domain,int Type,int Protocol)
		:m_rand(Port+Domain+Type+Protocol,999999){
			Properties.Domain=Domain;
			Properties.Type=Type;
			Properties.Protocol=Protocol;
			Properties.PortNumber=Port;
			Properties.Descriptor=socket(Properties.Domain,Properties.Type,Properties.Protocol);
			if(Properties.Descriptor==-1){
				perror("Failed to create socket.");
				exit(-1);
			}
			Properties.Sockaddr.sin_family=Properties.Domain;
			if(IP=="ANY"){
				Properties.Sockaddr.sin_addr.s_addr=INADDR_ANY;
			}else{
				Properties.Sockaddr.sin_addr.s_addr=inet_addr(IP.c_str());
			}
			Properties.Sockaddr.sin_port=htons(Properties.PortNumber);
			if(bind(Properties.Descriptor,(struct sockaddr *)&Properties.Sockaddr,sizeof(Properties.Sockaddr))<0){
				perror("Failed to bind");
				exit(-2);
			}
			if(listen(Properties.Descriptor,Properties.Backlog) != 0){
				perror("Failed to listen");
				exit(-3);
			}
		}
		size_t SocketServer_st::Accept(){
			Client_st Client;
			Client.SockaddrSize=sizeof(struct sockaddr_in);
			Client.Descriptor=accept(Properties.Descriptor,(struct sockaddr *)&Client.Sockaddr,(socklen_t*)&Client.SockaddrSize);
			if(Client.Descriptor<0){
				perror("Failed to accept client");
				exit(-4);
			}
			return AddClient(Client);
		}
		size_t SocketServer_st::AddClient(Client_st& Client){
			Client.LocalID=m_rand.Generate_Int();
			//TODO: Check if the generated ID already exists as a client (if so generate a new one).
			Clients.push_back(Client);
			return Client.LocalID;
		}
		void SocketServer_st::RemoveClient(size_t LocalID){
			for(size_t i=0;i<Clients.size();i++){
				if(Clients[i].LocalID==LocalID){
					close(Clients[i].Descriptor);
					Clients.erase(Clients.begin()+i);
					return;
				}
			}
		}
		std::string SocketServer_st::GetIP(size_t LocalID){
			Client_st Client;
			for(size_t i=0;i<Clients.size();i++){
				if(Clients[i].LocalID==LocalID){
					Client=Clients[i];
					break;
				}
			}
			char *client_ip = inet_ntoa(Client.Sockaddr.sin_addr);
			int client_port = ntohs(Client.Sockaddr.sin_port);
			return std::string(client_ip)+std::string(":")+std::to_string(client_port);
		}
		void SocketServer_st::Send(size_t LocalID,std::string msg){
			Client_st Client;
			for(size_t i=0;i<Clients.size();i++){
				if(Clients[i].LocalID==LocalID){
					Client=Clients[i];
					break;
				}
			}
			if(write(Client.Descriptor,msg.c_str(),msg.size())<0){
				perror("Failed write to client.");
				exit(-5);
			}
		}
		std::string SocketServer_st::Receive(size_t LocalID,size_t bufferSize){
			char* replyBuffer = new char[bufferSize+1];
			memset(replyBuffer,0,bufferSize+1);
			Client_st Client;
			for(size_t i=0;i<Clients.size();i++){
				if(Clients[i].LocalID==LocalID){
					Client=Clients[i];
					break;
				}
			}
			if(Client.LocalID==0){
				perror("Failed to find connected client by id");
				exit(-6);
			}
         if(recv(Client.Descriptor,replyBuffer,bufferSize,0) < 0){
				perror("Failed to receive data");
            exit(-7);
         }
         if(replyBuffer[bufferSize+1] != 0){
				printf("??%x",replyBuffer[bufferSize+1]);
				perror("Overflow!");
            exit(-8);
         }
         std::string reply(replyBuffer);
         memset(replyBuffer,0,bufferSize+1);
         delete[] replyBuffer;
			return reply;
		}
	};
};
