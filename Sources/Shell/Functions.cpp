#include "Shell/Functions.hpp"
#include <cmath>
#include <string>
#include <algorithm>
#include "Utilities/Choas.hpp"

#include <iostream>
#include <cassert>

namespace Luna{
    namespace Shell{
		namespace Functions{
			//standard math
			Value_st* F_add(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				Value_st* Result{nullptr};
				size_t Index=0;
				while(Index < Arguments.size()){
					switch(Arguments.at(Index)->Type()){
						case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// int int
											int64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(IntegerValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int int
											int64_t V = Result->As_Integer()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Integer()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int int
											int64_t V = Result->As_Bool()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float int int
											std::float64_t V = Result->As_Float()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Integer()->m_number=V;
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// int bool
											int64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int bool
											int64_t V = Result->As_Integer()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int bool
											int64_t V = Result->As_Bool()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float int bool
											std::float64_t V = Result->As_Float()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// int float
											std::float64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int float
											std::float64_t V = Result->As_Integer()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int float
											int64_t V = Result->As_Bool()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											//float int float
											std::float64_t V = Result->As_Float()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null int null
									int64_t V = Arguments.at(Index)->As_Integer()->m_number;
									Result = Arena_Allocator.Borrow(IntegerValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int int null
									int64_t V = Result->As_Integer()->m_number;
									V+=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Integer()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool int null
									int64_t V = Result->As_Bool()->m_number;
									V+=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float int null
									std::float64_t V = Result->As_Float()->m_number;
									V+=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Integer()->m_number=V;
									Index+=1;
								}
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// bool int
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool int
											int64_t V = Result->As_Integer()->m_number;
											V+=Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool int
											int64_t V = Result->As_Bool()->m_number;
											V+=Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float bool int
											std::float64_t V = Result->As_Float()->m_number;
											V+=Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// bool bool
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool bool
											int64_t V = Result->As_Integer()->m_number;
											V+=Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool bool
											int64_t V = Result->As_Bool()->m_number;
											V+=Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float bool bool
											std::float64_t V = Result->As_Float()->m_number;
											V+=Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// bool float
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool float
											int64_t V = Result->As_Integer()->m_number;
											V+=Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool float
											std::float64_t V = Result->As_Bool()->m_number;
											V+=Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											//float bool float
											std::float64_t V = Result->As_Float()->m_number;
											V+=Arguments.at(Index)->As_Bool()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null bool null
									int64_t V = Arguments.at(Index)->As_Bool()->m_number;
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int bool null
									int64_t V = Result->As_Integer()->m_number;
									V+=Arguments.at(Index)->As_Bool()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool bool null
									int64_t V = Result->As_Bool()->m_number;
									V+=Arguments.at(Index)->As_Bool()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float bool null
									std::float64_t V = Result->As_Float()->m_number;
									V+=Arguments.at(Index)->As_Bool()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// float int
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float int
											std::float64_t V = Result->As_Integer()->m_number;
											V+=Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float int
											int64_t V = Result->As_Bool()->m_number;
											V+=Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float int
											std::float64_t V = Result->As_Float()->m_number;
											V+=Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// float bool
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float bool
											int64_t V = Result->As_Integer()->m_number;
											V+=Arguments.at(Index)->As_Integer()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float bool
											int64_t V = Result->As_Bool()->m_number;
											V+=Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float bool
											std::float64_t V = Result->As_Float()->m_number;
											V+=Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// float float
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float float
											std::float64_t V = Result->As_Integer()->m_number;
											V+=Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float float
											int64_t V = Result->As_Bool()->m_number;
											V+=Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float float
											std::float64_t V = Result->As_Float()->m_number;
											V+=Arguments.at(Index)->As_Float()->m_number;
											V+=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null float null
									std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
									Result = Arena_Allocator.Borrow(FloatValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int float null
									std::float64_t V = Result->As_Integer()->m_number;
									V+=Arguments.at(Index)->As_Float()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(FloatValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool float null
									int64_t V = Result->As_Bool()->m_number;
									V+=Arguments.at(Index)->As_Float()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float float null
									std::float64_t V = Result->As_Float()->m_number;
									V+=Arguments.at(Index)->As_Float()->m_number;
									Result->As_Float()->m_number=V;
									Index+=1;
								}
							}
						}break;
						default:{
							std::string msg="Unimplemented node type in add function, ";
							msg+=Arguments.at(Index)->String();
							msg+=".";
							return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
						}break;
					}
				}
				if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
					std::float64_t V1 = Result->As_Float()->m_number;
					if(!(int)(std::fmod(V1,1.0)*100000)){
						Arena_Allocator.Return(Result);
						Result = Arena_Allocator.Borrow(IntegerValue_st((int64_t)V1));
					}
				}
				return Result;
			}
			Value_st* F_subtract(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				Value_st* Result{nullptr};
				size_t Index=0;
				while(Index < Arguments.size()){
					switch(Arguments.at(Index)->Type()){
						case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// int int
											int64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(IntegerValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int int
											int64_t V = Result->As_Integer()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Integer()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int int
											int64_t V = Result->As_Bool()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float int int
											std::float64_t V = Result->As_Float()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Integer()->m_number=V;
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// int bool
											int64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int bool
											int64_t V = Result->As_Integer()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int bool
											int64_t V = Result->As_Bool()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float int bool
											std::float64_t V = Result->As_Float()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// int float
											std::float64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int float
											std::float64_t V = Result->As_Integer()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int float
											int64_t V = Result->As_Bool()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											//float int float
											std::float64_t V = Result->As_Float()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null int null
									int64_t V = Arguments.at(Index)->As_Integer()->m_number;
									Result = Arena_Allocator.Borrow(IntegerValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int int null
									int64_t V = Result->As_Integer()->m_number;
									V-=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Integer()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool int null
									int64_t V = Result->As_Bool()->m_number;
									V-=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float int null
									std::float64_t V = Result->As_Float()->m_number;
									V-=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Integer()->m_number=V;
									Index+=1;
								}
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// bool int
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool int
											int64_t V = Result->As_Integer()->m_number;
											V-=Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool int
											int64_t V = Result->As_Bool()->m_number;
											V-=Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float bool int
											std::float64_t V = Result->As_Float()->m_number;
											V-=Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// bool bool
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool bool
											int64_t V = Result->As_Integer()->m_number;
											V-=Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool bool
											int64_t V = Result->As_Bool()->m_number;
											V-=Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float bool bool
											std::float64_t V = Result->As_Float()->m_number;
											V-=Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// bool float
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool float
											int64_t V = Result->As_Integer()->m_number;
											V-=Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool float
											std::float64_t V = Result->As_Bool()->m_number;
											V-=Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											//float bool float
											std::float64_t V = Result->As_Float()->m_number;
											V-=Arguments.at(Index)->As_Bool()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null bool null
									int64_t V = Arguments.at(Index)->As_Bool()->m_number;
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int bool null
									int64_t V = Result->As_Integer()->m_number;
									V-=Arguments.at(Index)->As_Bool()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool bool null
									int64_t V = Result->As_Bool()->m_number;
									V-=Arguments.at(Index)->As_Bool()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float bool null
									std::float64_t V = Result->As_Float()->m_number;
									V-=Arguments.at(Index)->As_Bool()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// float int
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float int
											std::float64_t V = Result->As_Integer()->m_number;
											V-=Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float int
											int64_t V = Result->As_Bool()->m_number;
											V-=Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float int
											std::float64_t V = Result->As_Float()->m_number;
											V-=Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// float bool
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float bool
											int64_t V = Result->As_Integer()->m_number;
											V-=Arguments.at(Index)->As_Integer()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float bool
											int64_t V = Result->As_Bool()->m_number;
											V-=Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float bool
											std::float64_t V = Result->As_Float()->m_number;
											V-=Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// float float
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float float
											std::float64_t V = Result->As_Integer()->m_number;
											V-=Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float float
											int64_t V = Result->As_Bool()->m_number;
											V-=Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float float
											std::float64_t V = Result->As_Float()->m_number;
											V-=Arguments.at(Index)->As_Float()->m_number;
											V-=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null float null
									std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
									Result = Arena_Allocator.Borrow(FloatValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int float null
									std::float64_t V = Result->As_Integer()->m_number;
									V-=Arguments.at(Index)->As_Float()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(FloatValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool float null
									int64_t V = Result->As_Bool()->m_number;
									V-=Arguments.at(Index)->As_Float()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float float null
									std::float64_t V = Result->As_Float()->m_number;
									V-=Arguments.at(Index)->As_Float()->m_number;
									Result->As_Float()->m_number=V;
									Index+=1;
								}
							}
						}break;
						default:{
							std::string msg="Unimplemented node type in add function, ";
							msg+=Arguments.at(Index)->String();
							msg+=".";
							return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
						}break;
					}
				}
				if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
					std::float64_t V1 = Result->As_Float()->m_number;
					if(!(int)(std::fmod(V1,1.0)*100000)){
						Arena_Allocator.Return(Result);
						Result = Arena_Allocator.Borrow(IntegerValue_st((int64_t)V1));
					}
				}
				return Result;
			}
			Value_st* F_multiply(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				Value_st* Result{nullptr};
				size_t Index=0;
				while(Index < Arguments.size()){
					switch(Arguments.at(Index)->Type()){
						case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// int int
											int64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(IntegerValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int int
											int64_t V = Result->As_Integer()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Integer()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int int
											int64_t V = Result->As_Bool()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float int int
											std::float64_t V = Result->As_Float()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Integer()->m_number=V;
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// int bool
											int64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int bool
											int64_t V = Result->As_Integer()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int bool
											int64_t V = Result->As_Bool()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float int bool
											std::float64_t V = Result->As_Float()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// int float
											std::float64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int float
											std::float64_t V = Result->As_Integer()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int float
											int64_t V = Result->As_Bool()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											//float int float
											std::float64_t V = Result->As_Float()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null int null
									int64_t V = Arguments.at(Index)->As_Integer()->m_number;
									Result = Arena_Allocator.Borrow(IntegerValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int int null
									int64_t V = Result->As_Integer()->m_number;
									V*=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Integer()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool int null
									int64_t V = Result->As_Bool()->m_number;
									V*=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float int null
									std::float64_t V = Result->As_Float()->m_number;
									V*=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Integer()->m_number=V;
									Index+=1;
								}
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// bool int
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool int
											int64_t V = Result->As_Integer()->m_number;
											V*=Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool int
											int64_t V = Result->As_Bool()->m_number;
											V*=Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float bool int
											std::float64_t V = Result->As_Float()->m_number;
											V*=Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// bool bool
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool bool
											int64_t V = Result->As_Integer()->m_number;
											V*=Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool bool
											int64_t V = Result->As_Bool()->m_number;
											V*=Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float bool bool
											std::float64_t V = Result->As_Float()->m_number;
											V*=Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// bool float
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool float
											int64_t V = Result->As_Integer()->m_number;
											V*=Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool float
											std::float64_t V = Result->As_Bool()->m_number;
											V*=Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											//float bool float
											std::float64_t V = Result->As_Float()->m_number;
											V*=Arguments.at(Index)->As_Bool()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null bool null
									int64_t V = Arguments.at(Index)->As_Bool()->m_number;
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int bool null
									int64_t V = Result->As_Integer()->m_number;
									V*=Arguments.at(Index)->As_Bool()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool bool null
									int64_t V = Result->As_Bool()->m_number;
									V*=Arguments.at(Index)->As_Bool()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float bool null
									std::float64_t V = Result->As_Float()->m_number;
									V*=Arguments.at(Index)->As_Bool()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// float int
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float int
											std::float64_t V = Result->As_Integer()->m_number;
											V*=Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float int
											int64_t V = Result->As_Bool()->m_number;
											V*=Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float int
											std::float64_t V = Result->As_Float()->m_number;
											V*=Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// float bool
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float bool
											int64_t V = Result->As_Integer()->m_number;
											V*=Arguments.at(Index)->As_Integer()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float bool
											int64_t V = Result->As_Bool()->m_number;
											V*=Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float bool
											std::float64_t V = Result->As_Float()->m_number;
											V*=Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// float float
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float float
											std::float64_t V = Result->As_Integer()->m_number;
											V*=Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float float
											int64_t V = Result->As_Bool()->m_number;
											V*=Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float float
											std::float64_t V = Result->As_Float()->m_number;
											V*=Arguments.at(Index)->As_Float()->m_number;
											V*=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null float null
									std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
									Result = Arena_Allocator.Borrow(FloatValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int float null
									std::float64_t V = Result->As_Integer()->m_number;
									V*=Arguments.at(Index)->As_Float()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(FloatValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool float null
									int64_t V = Result->As_Bool()->m_number;
									V*=Arguments.at(Index)->As_Float()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float float null
									std::float64_t V = Result->As_Float()->m_number;
									V*=Arguments.at(Index)->As_Float()->m_number;
									Result->As_Float()->m_number=V;
									Index+=1;
								}
							}
						}break;
						default:{
							std::string msg="Unimplemented node type in add function, ";
							msg+=Arguments.at(Index)->String();
							msg+=".";
							return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
						}break;
					}
				}
				if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
					std::float64_t V1 = Result->As_Float()->m_number;
					if(!(int)(std::fmod(V1,1.0)*100000)){
						Arena_Allocator.Return(Result);
						Result = Arena_Allocator.Borrow(IntegerValue_st((int64_t)V1));
					}
				}
				return Result;
			}
			Value_st* F_divide(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				Value_st* Result{nullptr};
				size_t Index=0;
				while(Index < Arguments.size()){
					switch(Arguments.at(Index)->Type()){
						case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// int int
											if(Arguments.at(Index)->As_Integer()->m_number < Arguments.at(1+Index)->As_Integer()->m_number){
												std::float64_t V = Arguments.at(Index)->As_Integer()->m_number;
												V/=Arguments.at(1+Index)->As_Integer()->m_number;
												Result = Arena_Allocator.Borrow(FloatValue_st(V));
												Index+=2;
											}else{
												int64_t V = Arguments.at(Index)->As_Integer()->m_number;
												V/=Arguments.at(1+Index)->As_Integer()->m_number;
												Result = Arena_Allocator.Borrow(IntegerValue_st(V));
												Index+=2;
											}
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int int
											int64_t V = Result->As_Integer()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Integer()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int int
											int64_t V = Result->As_Bool()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float int int
											std::float64_t V = Result->As_Float()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Integer()->m_number=V;
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// int bool
											int64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int bool
											int64_t V = Result->As_Integer()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int bool
											int64_t V = Result->As_Bool()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float int bool
											std::float64_t V = Result->As_Float()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// int float
											std::float64_t V = Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int int float
											std::float64_t V = Result->As_Integer()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool int float
											int64_t V = Result->As_Bool()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											//float int float
											std::float64_t V = Result->As_Float()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null int null
									int64_t V = Arguments.at(Index)->As_Integer()->m_number;
									Result = Arena_Allocator.Borrow(IntegerValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int int null
									int64_t V = Result->As_Integer()->m_number;
									V/=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Integer()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool int null
									int64_t V = Result->As_Bool()->m_number;
									V/=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float int null
									std::float64_t V = Result->As_Float()->m_number;
									V/=Arguments.at(Index)->As_Integer()->m_number;
									Result->As_Integer()->m_number=V;
									Index+=1;
								}
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// bool int
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool int
											int64_t V = Result->As_Integer()->m_number;
											V/=Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool int
											int64_t V = Result->As_Bool()->m_number;
											V/=Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float bool int
											std::float64_t V = Result->As_Float()->m_number;
											V/=Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// bool bool
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool bool
											int64_t V = Result->As_Integer()->m_number;
											V/=Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool bool
											int64_t V = Result->As_Bool()->m_number;
											V/=Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float bool bool
											std::float64_t V = Result->As_Float()->m_number;
											V/=Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// bool float
											int64_t V = Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int bool float
											int64_t V = Result->As_Integer()->m_number;
											V/=Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool bool float
											std::float64_t V = Result->As_Bool()->m_number;
											V/=Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											//float bool float
											std::float64_t V = Result->As_Float()->m_number;
											V/=Arguments.at(Index)->As_Bool()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null bool null
									int64_t V = Arguments.at(Index)->As_Bool()->m_number;
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int bool null
									int64_t V = Result->As_Integer()->m_number;
									V/=Arguments.at(Index)->As_Bool()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool bool null
									int64_t V = Result->As_Bool()->m_number;
									V/=Arguments.at(Index)->As_Bool()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float bool null
									std::float64_t V = Result->As_Float()->m_number;
									V/=Arguments.at(Index)->As_Bool()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(BoolValue_st(V));
									Index+=1;
								}
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
							if(1+Index < Arguments.size()){
								switch(Arguments.at(1+Index)->Type()){
									case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
										if(Result == nullptr){
											// float int
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float int
											std::float64_t V = Result->As_Integer()->m_number;
											V/=Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float int
											int64_t V = Result->As_Bool()->m_number;
											V/=Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float int
											std::float64_t V = Result->As_Float()->m_number;
											V/=Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Integer()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
										if(Result == nullptr){
											// float bool
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float bool
											int64_t V = Result->As_Integer()->m_number;
											V/=Arguments.at(Index)->As_Integer()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float bool
											int64_t V = Result->As_Bool()->m_number;
											V/=Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float bool
											std::float64_t V = Result->As_Float()->m_number;
											V/=Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Bool()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(BoolValue_st(V));
											Index+=2;
										}
									}break;
									case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
										if(Result == nullptr){
											// float float
											std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
											// int float float
											std::float64_t V = Result->As_Integer()->m_number;
											V/=Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Arena_Allocator.Return(Result);
											Result = Arena_Allocator.Borrow(FloatValue_st(V));
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
											// bool float float
											int64_t V = Result->As_Bool()->m_number;
											V/=Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Bool()->m_number=V;
											Index+=2;
										}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
											// float float float
											std::float64_t V = Result->As_Float()->m_number;
											V/=Arguments.at(Index)->As_Float()->m_number;
											V/=Arguments.at(1+Index)->As_Float()->m_number;
											Result->As_Float()->m_number=V;
											Index+=2;
										}
									}break;
									default:{
										std::string msg="Unimplemented node type in add function, ";
										msg+=Arguments.at(1+Index)->String();
										msg+=".";
										return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
									}break;
								}
							}else{
								if(Result == nullptr){
									// null float null
									std::float64_t V = Arguments.at(Index)->As_Float()->m_number;
									Result = Arena_Allocator.Borrow(FloatValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
									// int float null
									std::float64_t V = Result->As_Integer()->m_number;
									V/=Arguments.at(Index)->As_Float()->m_number;
									Arena_Allocator.Return(Result);
									Result = Arena_Allocator.Borrow(FloatValue_st(V));
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
									// bool float null
									int64_t V = Result->As_Bool()->m_number;
									V/=Arguments.at(Index)->As_Float()->m_number;
									Result->As_Bool()->m_number=V;
									Index+=1;
								}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
									// float float null
									std::float64_t V = Result->As_Float()->m_number;
									V/=Arguments.at(Index)->As_Float()->m_number;
									Result->As_Float()->m_number=V;
									Index+=1;
								}
							}
						}break;
						default:{
							std::string msg="Unimplemented node type in add function, ";
							msg+=Arguments.at(Index)->String();
							msg+=".";
							return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
						}break;
					}
				}
				if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
					std::float64_t V1 = Result->As_Float()->m_number;
					if(!(int)(std::fmod(V1,1.0)*100000)){
						Arena_Allocator.Return(Result);
						Result = Arena_Allocator.Borrow(IntegerValue_st((int64_t)V1));
					}
				}
				return Result;
			}
			//comparison
			Value_st* F_greater(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());
				for(size_t i=1;i<Arguments.size();i++){
					Value_st* LHS = Arguments.at(i-1);
					Value_st* RHS = Arguments.at(i);
					std::deque<Value_st*> tmpargs = {LHS,RHS};
					auto Result = F_subtract(tmpargs,Arena_Allocator);
					if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
						if(Result->As_Integer()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else if(Result->As_Integer()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
						if(Result->As_Bool()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else if(Result->As_Bool()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
						if(Result->As_Float()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else if(Result->As_Float()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}
					}
				}
				return List;
			}
			Value_st* F_lesser(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());
				for(size_t i=1;i<Arguments.size();i++){
					Value_st* LHS = Arguments.at(i-1);
					Value_st* RHS = Arguments.at(i);
					std::deque<Value_st*> tmpargs = {LHS,RHS};
					auto Result = F_subtract(tmpargs,Arena_Allocator);
					if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
						if(Result->As_Integer()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else if(Result->As_Integer()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
						if(Result->As_Bool()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else if(Result->As_Bool()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
						if(Result->As_Float()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else if(Result->As_Float()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}
					}
				}
				return List;
			}
			Value_st* F_equivalent(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());
				for(size_t i=1;i<Arguments.size();i++){
					Value_st* LHS = Arguments.at(i-1);
					Value_st* RHS = Arguments.at(i);
					std::deque<Value_st*> tmpargs = {LHS,RHS};
					auto Result = F_subtract(tmpargs,Arena_Allocator);
					if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
						if(Result->As_Integer()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else if(Result->As_Integer()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
						if(Result->As_Bool()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else if(Result->As_Bool()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
						if(Result->As_Float()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else if(Result->As_Float()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}
					}
				}
				return List;
			}
			Value_st* F_and(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){//??...
				ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());
				for(size_t i=1;i<Arguments.size();i++){
					Value_st* LHS = Arguments.at(i-1);
					Value_st* RHS = Arguments.at(i);
					BoolValue_st* BLHS{nullptr};
					BoolValue_st* BRHS{nullptr};
					if(LHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
						BLHS = Arena_Allocator.Borrow(BoolValue_st(LHS->As_Integer()->m_number));
					}else if(LHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
						BLHS = Arena_Allocator.Borrow(BoolValue_st(LHS->As_Bool()->m_number));
					}else if(LHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
						BLHS = Arena_Allocator.Borrow(BoolValue_st(LHS->As_Float()->m_number));
					}
					if(RHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
						BRHS = Arena_Allocator.Borrow(BoolValue_st(RHS->As_Integer()->m_number));
					}else if(RHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
						BRHS = Arena_Allocator.Borrow(BoolValue_st(RHS->As_Bool()->m_number));
					}else if(RHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
						BRHS = Arena_Allocator.Borrow(BoolValue_st(RHS->As_Float()->m_number));
					}
					if(BLHS->String() == BRHS->String()){
						List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
					}else{
						List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
					}
				}
				return List;
			}
			Value_st* F_or(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){//??...
				ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());
				for(size_t i=1;i<Arguments.size();i++){
					Value_st* LHS = Arguments.at(i-1);
					Value_st* RHS = Arguments.at(i);
					BoolValue_st* BLHS{nullptr};
					BoolValue_st* BRHS{nullptr};
					if(LHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
						BLHS = Arena_Allocator.Borrow(BoolValue_st(LHS->As_Integer()->m_number));
					}else if(LHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
						BLHS = Arena_Allocator.Borrow(BoolValue_st(LHS->As_Bool()->m_number));
					}else if(LHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
						BLHS = Arena_Allocator.Borrow(BoolValue_st(LHS->As_Float()->m_number));
					}
					if(RHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
						BRHS = Arena_Allocator.Borrow(BoolValue_st(RHS->As_Integer()->m_number));
					}else if(RHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
						BRHS = Arena_Allocator.Borrow(BoolValue_st(RHS->As_Bool()->m_number));
					}else if(RHS->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
						BRHS = Arena_Allocator.Borrow(BoolValue_st(RHS->As_Float()->m_number));
					}
					if(BLHS->String() == "TRUE"){
						List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
					}else if(BRHS->String() == "TRUE"){
						List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
					}else{
						List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
					}
				}
				return List;
			}
			Value_st* F_equivalent_or_greater(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());
				for(size_t i=1;i<Arguments.size();i++){
					Value_st* LHS = Arguments.at(i-1);
					Value_st* RHS = Arguments.at(i);
					std::deque<Value_st*> tmpargs = {LHS,RHS};
					auto Result = F_subtract(tmpargs,Arena_Allocator);
					if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
						if(Result->As_Integer()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else if(Result->As_Integer()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
						if(Result->As_Bool()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else if(Result->As_Bool()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
						if(Result->As_Float()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else if(Result->As_Float()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}
					}
				}
				return List;
			}
			Value_st* F_equivalent_or_lesser(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());
				for(size_t i=1;i<Arguments.size();i++){
					Value_st* LHS = Arguments.at(i-1);
					Value_st* RHS = Arguments.at(i);
					std::deque<Value_st*> tmpargs = {LHS,RHS};
					auto Result = F_subtract(tmpargs,Arena_Allocator);
					if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
						if(Result->As_Integer()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else if(Result->As_Integer()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Bool){
						if(Result->As_Bool()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else if(Result->As_Bool()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}
					}else if(Result->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
						if(Result->As_Float()->m_number > 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(-1)));
						}else if(Result->As_Float()->m_number < 0){
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}else{
							List->Push_back(Arena_Allocator.Borrow(BoolValue_st(1)));
						}
					}
				}
				return List;
			}
			//experimental
			Value_st* F_selection(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				//(!SELECT (!DIVIDE 1 5) (5 3 4 8 1))
				if(Arguments.size() != 2)return Arena_Allocator.Borrow(ErrorValue_st("select requires a selector value and values to select from.",-1,-1));
				Value_st* S = Arguments.front();Arguments.pop_front();
				Value_st* L = Arguments.front();Arguments.pop_front();
				if(L->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_List){
					std::string msg="List expected, got ";
					msg+=Luna::Shell::ValueType_en::Str(L->Type());
					return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
				}
				Luna::Utilities::Choas_st<Value_st*> Options(L->As_List()->m_list);
				std::float64_t Selector=0.000000;
				if(S->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Float){
					Selector=S->As_Float()->m_number;
				}else if(S->Type() == Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
					Selector=S->As_Integer()->m_number;
				}
				return Options.Pull(Selector);
			}
			Value_st* F_sort(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){// 2 1
				std::deque<Value_st*> SortedArguments;
				for(size_t iA=0;iA<Arguments.size();iA++){
					bool Inserted=false;
					for(size_t iB=0;iB<SortedArguments.size();iB++){
						std::string Bstring = SortedArguments.at(iB)->String();
						std::string Astring = Arguments.at(iA)->String();
						std::deque<std::string> Test{Bstring,Astring};
						std::sort(Test.begin(), Test.end());
						if(Test.at(1) == Bstring){
							SortedArguments.insert(SortedArguments.begin()+iB,Arguments.at(iA));
							Inserted=true;
							break;
						}
					}
					if(!Inserted)SortedArguments.push_back(Arguments.at(iA));
				}
				ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());
				List->m_list = SortedArguments;
				return List;
			}
			Value_st* F_size(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				size_t Sizea=0;
				for(size_t i=0;i<Arguments.size();i++){
					switch(Arguments.at(i)->Type()){
						case Luna::Shell::ValueType_en::ValueType_en::Type_List:{
							Sizea+=Arguments.at(i)->As_List()->size();
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Vector:{
							Sizea+=Arguments.at(i)->As_Vector()->size();
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Hashmap:{
							Sizea+=Arguments.at(i)->As_HashMap()->size();
						}break;
						default:{
							Sizea++;
						}break;
					}
				}
				return Arena_Allocator.Borrow(IntegerValue_st(Sizea));
			}
			Value_st* F_recursive_size(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				size_t Sizea=0;
				for(size_t i=0;i<Arguments.size();i++){
					switch(Arguments.at(i)->Type()){
						case Luna::Shell::ValueType_en::ValueType_en::Type_List:{
							Sizea+=Arguments.at(i)->As_List()->recursive_size();
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Vector:{
							Sizea+=Arguments.at(i)->As_Vector()->recursive_size();
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Hashmap:{
							Sizea+=Arguments.at(i)->As_HashMap()->recursive_size();
						}break;
						default:{
							Sizea++;
						}break;
					}
				}
				return Arena_Allocator.Borrow(IntegerValue_st(Sizea));
			}
			Value_st* F_print(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				for(auto unit : Arguments){
					switch(unit->Type()){
						case Luna::Shell::ValueType_en::ValueType_en::Type_List:{
							for(auto element : unit->As_List()->m_list){
								std::cout << element->String() << " ";
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Vector:{
							for(auto element : unit->As_Vector()->m_vector){
								std::cout << element->String() << " ";
							}
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Hashmap:{
							for(auto element : unit->As_HashMap()->m_map){
								std::cout << element.first->String() << " ";
								std::cout << element.second->String() << " ";
							}
						}break;
						default:{
							std::cout << unit->String() << " ";
						}break;
					}
				}
				
				std::cout << std::endl;
				return Arena_Allocator.Borrow(BoolValue_st(0));
			}

			std::deque<std::string> G_recombinator(ListValue_st* InputLists){
				std::deque<std::string> result;
				std::deque<size_t> counters;
				for(size_t i=0;i<InputLists->size();i++){
					counters.push_back(0);
				}
				bool done=false;
				do{
					std::string tmpresult={""};
					if(!done)for(size_t i=0;i<counters.size();i++){
						tmpresult+=InputLists->m_list.at(i)->As_List()->m_list.at(counters.at(i))->String();
					}
					result.push_back(tmpresult);
					done=true;
					for(size_t i=0;i<counters.size();i++){
						if(counters.at(i)+1 < InputLists->m_list.at(i)->As_List()->size())done=false;
					}
					counters.at(counters.size()-1)++;
					if(counters.at(counters.size()-1) >= InputLists->m_list.at(InputLists->size()-1)->As_List()->size()){
						for(size_t i=counters.size()-1;i>0;i--){
							if(counters.at(i) >= InputLists->m_list.at(i)->As_List()->size() && (counters.at(i-1) < InputLists->m_list.at(i-1)->As_List()->size())){
								counters.at(i)=0;
								counters.at(i-1)++;
							}
						}
					}
				}while(!done);
				return result;
			}

			Value_st* F_recombinator(std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
				ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());

				for(auto element : Arguments){
					switch(element->Type()){
						case Luna::Shell::ValueType_en::ValueType_en::Type_List:{
							List->Push_back(element->As_List());
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Vector:{
							ListValue_st* V = Arena_Allocator.Borrow(ListValue_st());
							for(auto selement : element->As_Vector()->m_vector){
								V->Push_back(selement->Duplicate(Arena_Allocator));
							}
							List->Push_back(V);
						}break;
						case Luna::Shell::ValueType_en::ValueType_en::Type_Hashmap:{
							ListValue_st* V = Arena_Allocator.Borrow(ListValue_st());
							for(auto& selement : element->As_HashMap()->m_map){
								V->Push_back(selement.first->Duplicate(Arena_Allocator));
								V->Push_back(selement.second->Duplicate(Arena_Allocator));
							}
							List->Push_back(V);
						}break;
						default:{
							ListValue_st* V = Arena_Allocator.Borrow(ListValue_st());
							V->Push_back(element->Duplicate(Arena_Allocator));
							List->Push_back(V);
						}break;
					}
				}
				std::deque<std::string> GList = G_recombinator(List);
				List->m_list.clear();
				for(auto element : GList){
					auto Gelement = Arena_Allocator.Borrow(SymbolValue_st(element));
					List->Push_back(Gelement);
				}
				
				return List;
			}
		};
	};
};