#include "Shell/Interface.hpp"
#include "Shell/Reader.hpp"
#include "Shell/Printer.hpp"


#include <iostream>

namespace Luna{
    namespace Shell{
        Value_st* Interface_cl::eval_ast(Value_st* Input,Enviroment_cl* Env){
            std::cerr<<"?eval_ast?" << Input->String() << std::endl;
            switch(Input->Type()){
                case Luna::Shell::ValueType_en::ValueType_en::Type_Symbol:{
                    Value_st* R = Env->Get(Input->As_Symbol());
                    if(R == nullptr)R = Input;
                    std::cerr<<"!eval_ast!" << R->String() << std::endl;
                    return R;
                }break;
                case Luna::Shell::ValueType_en::ValueType_en::Type_List:{
                    ListValue_st* List = Arena_Allocator.Borrow(ListValue_st());
                    for(auto element : Input->As_List()->m_list){
                        List->Push_back(EVAL(element,Env));
                    }
                    std::cerr<<"!eval_ast!" << List->String() << std::endl;
                    return List;
                }break;
                case Luna::Shell::ValueType_en::ValueType_en::Type_Vector:{
                    VectorValue_st* Vector = Arena_Allocator.Borrow(VectorValue_st());
                    for(auto element : Input->As_Vector()->m_vector){
                        Vector->Push_back(EVAL(element,Env));
                    }
                    std::cerr<<"!eval_ast!" << Vector->String() << std::endl;
                    return Vector;
                }break;
                case Luna::Shell::ValueType_en::ValueType_en::Type_Hashmap:{
                    HashMapValue_st* map = Arena_Allocator.Borrow(HashMapValue_st());
                    for(auto element : Input->As_HashMap()->m_map){
                        map->Set(element.first,EVAL(element.second,Env));
                    }
                    std::cerr<<"!eval_ast!" << map->String() << std::endl;
                    return map;
                }break;
                default:break;
            }
            return Input;
        }
    };
    namespace Shell{
        Value_st* Interface_cl::READ(std::string Input){
            return Read_str(Input);
        }
        Value_st* Interface_cl::EVAL(Value_st* Input,Enviroment_cl* Env){
            std::cerr<<"?EVAL?" << Input->String() << std::endl;
            switch(Input->Type()){
                case Luna::Shell::ValueType_en::ValueType_en::Type_List:{
                    if(!Input->As_List()->m_list.size())return Input;
                    switch(Input->As_List()->Front()->Type()){
                        case Luna::Shell::ValueType_en::ValueType_en::Type_Symbol:{
                            SymbolValue_st* Sfirst = Input->As_List()->Front()->As_Symbol();
                            if(Sfirst->m_symbol == "!DEFINE"){
                                Input->As_List()->Pop_front();
                                auto Skey = Input->As_List()->Pop_front();
                                if(Skey->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_Symbol){
                                    std::string msg = "Expected symbol, got ";
                                    msg+=Luna::Shell::ValueType_en::Str(Skey->Type());
                                    return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
                                }
                                auto Svalue = EVAL(Input->As_List()->Pop_front(),Env);
                                Env->Set(Skey->As_Symbol(),Svalue);
                                std::cerr<<"!EVAL!" << Svalue->String() << std::endl;
                                return Svalue;
                            }
                            else if(Sfirst->m_symbol == "!LET"){
                                Input->As_List()->Pop_front();
                                Enviroment_cl* nEnv = Arena_Allocator.Borrow(Enviroment_cl(Env));
                                auto bindings = Input->As_List()->Pop_front();
                                if(bindings->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_List){
                                    std::string msg = "Expected list, got ";
                                    msg+=Luna::Shell::ValueType_en::Str(bindings->Type());
                                    return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
                                }
                                for(size_t i=1;i<bindings->As_List()->size();i+=2){
                                    auto Skey = bindings->As_List()->m_list.at(i-1);
                                    if(Skey->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_Symbol){
                                        std::string msg = "Expected symbol, got ";
                                        msg+=Luna::Shell::ValueType_en::Str(Skey->Type());
                                        return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
                                    }
                                    auto Svalue = EVAL(bindings->As_List()->m_list.at(i),nEnv);
                                    nEnv->Set(Skey->As_Symbol(),Svalue);
                                }
                                auto R = EVAL(Input,nEnv);
                                Arena_Allocator.Return(nEnv);
                                std::cerr<<"!EVAL!" << R->String() << std::endl;
                                return R;
                            }
                            else if(Sfirst->m_symbol == "!DO"){
                                Input->As_List()->Pop_front();
                                Value_st* Result{nullptr};
                                for(size_t i=0;i<Input->As_List()->size();i++){
                                    Result = eval_ast(Input->As_List()->m_list.at(i),Env);
                                }
                                std::cerr<<"!EVAL!" << Result->String() << std::endl;
                                return Result;
                            }
                            else if(Sfirst->m_symbol == "!IF"){
                                Input->As_List()->Pop_front();
                                Value_st* unchecked_condition = Input->As_List()->Pop_front();
                                Value_st* True_Expr = Input->As_List()->Pop_front();
                                Value_st* False_Expr = Arena_Allocator.Borrow(BoolValue_st(0));
                                Value_st* Null_Expr = Arena_Allocator.Borrow(BoolValue_st(0));
                                if(Input->As_List()->size()){
                                    Arena_Allocator.Return(False_Expr);
                                    False_Expr = Input->As_List()->Pop_front();
                                }
                                if(Input->As_List()->size()){
                                    Arena_Allocator.Return(Null_Expr);
                                    Null_Expr = Input->As_List()->Pop_front();
                                }
                                Value_st* condition = EVAL(unchecked_condition,Env);
                                Value_st* Result;
                                switch(condition->Type()){
                                    case Luna::Shell::ValueType_en::ValueType_en::Type_Bool:{
                                        if(condition->String() == "TRUE")Result = True_Expr;
                                        if(condition->String() == "FALSE")Result = False_Expr;
                                        Result = Null_Expr;
                                    }break;
                                    case Luna::Shell::ValueType_en::ValueType_en::Type_Integer:{
                                        Value_st* V = Arena_Allocator.Borrow(BoolValue_st(condition->As_Integer()->m_number));
                                        Arena_Allocator.Return(condition);
                                        condition = V;
                                        if(condition->String() == "TRUE")Result = True_Expr;
                                        if(condition->String() == "FALSE")Result = False_Expr;
                                        Result = Null_Expr;
                                    }break;
                                    case Luna::Shell::ValueType_en::ValueType_en::Type_Float:{
                                        Value_st* V = Arena_Allocator.Borrow(BoolValue_st(condition->As_Float()->m_number));
                                        Arena_Allocator.Return(condition);
                                        condition = V;
                                        if(condition->String() == "TRUE")Result = True_Expr;
                                        if(condition->String() == "FALSE")Result = False_Expr;
                                        Result = Null_Expr;
                                    }break;
                                    default:{Result = False_Expr;}break;
                                }
                                std::cerr<<"!EVAL!" << Result->String() << std::endl;
                                return Result;
                            }
                            else if(Sfirst->m_symbol == "!FUNCTION"){
                                Input->As_List()->Pop_front();
                                auto bindings = Input->As_List()->Pop_front();
                                if(bindings->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_List){
                                    std::string msg = "Expected list, got ";
                                    msg+=Luna::Shell::ValueType_en::Str(bindings->Type());
                                    return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
                                }
                                auto body = Input->As_List()->Pop_front();
                                if(body->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_List){
                                    std::string msg = "Expected list, got ";
                                    msg+=Luna::Shell::ValueType_en::Str(body->Type());
                                    return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
                                }
                                FnPtr closure = [=](std::deque<Value_st*> Arguments,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) -> Value_st* {
                                    Enviroment_cl* nEnv = Arena_Allocator.Borrow(Enviroment_cl(Env));
                                    ListValue_st* exprs = Arena_Allocator.Borrow(ListValue_st());
                                    for(size_t i=0;i<Arguments.size();i++){
                                        exprs->Push_back(Arguments.at(i));
                                    }
                                    if(exprs->As_List()->size() != bindings->As_List()->size()){
                                        std::string msg = "Expected ";
                                        msg+=std::to_string(bindings->As_List()->size());
                                        msg+=", got ";
                                        msg+=std::to_string(exprs->As_List()->size());
                                        return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
                                    }
                                    for(size_t i=0;i<exprs->As_List()->size();i++){
                                        auto Skey = bindings->As_List()->m_list.at(i);
                                        if(Skey->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_Symbol){
                                            std::string msg = "Expected symbol, got ";
                                            msg+=Luna::Shell::ValueType_en::Str(Skey->Type());
                                            return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
                                        }
                                        auto Svalue = EVAL(exprs->As_List()->m_list.at(i),nEnv);
                                        nEnv->Set(Skey->As_Symbol(),Svalue);
                                    }
                                    auto R = EVAL(body,nEnv);
                                    Arena_Allocator.Return(nEnv);
                                    return R;
                                };

                                auto R = Arena_Allocator.Borrow(FunctionValue_st(closure));
                                std::cerr<<"!EVAL!" << R->String() << std::endl;
                                return R;
                            }
                            else if(Sfirst->m_symbol == "!LOOP"){
                                Input->As_List()->Pop_front();
                                auto iterations = Input->As_List()->Pop_front();
                                if(iterations->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_Integer){
                                    std::string msg = "Expected integer, got ";
                                    msg+=Luna::Shell::ValueType_en::Str(iterations->Type());
                                    return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
                                }
                                Value_st* Result{nullptr};
                                for(size_t v=+iterations->As_Integer()->m_number;v;v--){
                                    auto Cbody = Input->Duplicate(Arena_Allocator);
                                    for(size_t i=0;i<Cbody->As_List()->size();i++){
                                        Result = eval_ast(Cbody->As_List()->m_list.at(i),Env);
                                    }
                                    Arena_Allocator.Return(Cbody);
                                }
                                std::cerr<<"!EVAL!" << Result->String() << std::endl;
                                return Result;
                            }
                            else if(Sfirst->m_symbol == "!RECOMBINATOR"){
                                Input->As_List()->Pop_front();
                                ListValue_st* GList = Luna::Shell::Functions::F_recombinator(Input->As_List()->m_list,Arena_Allocator)->As_List();
                                std::cerr<<"!EVAL!" << GList->String() << std::endl;
                                return GList;
                            }
                        }break;
                        default:break;
                    }
                    auto list = eval_ast(Input,Env);
                    if(list->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_List){
                        std::string msg = "Expected list, got ";
                        msg+=Luna::Shell::ValueType_en::Str(list->Type());
                        return Arena_Allocator.Borrow(ErrorValue_st(msg,-1,-1));
                    }
                    auto funvalue = list->As_List()->Front();
                    if(funvalue->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_Function){
                        return list;
                    }
                    list->As_List()->Pop_front();
                    return funvalue->As_Function()->Call(list->As_List()->m_list,Arena_Allocator);
                }break;
                default:break;
            }
            auto R = eval_ast(Input,Env);
            std::cerr<<"!EVAL!" << R->String() << std::endl;
            return R;
        }
        std::string Interface_cl::PRINT(Value_st* Input){return pr_str(Input);}
        
        std::string Interface_cl::Repl(std::string Input){
            auto A1 = READ(Input);
            auto A2 = EVAL(A1,RootEnviroment);
            std::string Output = PRINT(A2);
            //Arena_Allocator.Yeet();
            return Output;
        }
        Interface_cl::Interface_cl():Arena_Allocator(1024*1024*4/*4mb*/){
            RootEnviroment = Arena_Allocator.Borrow(Enviroment_cl(nullptr),0);
            //standard math
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!ADD"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_add),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!SUBTRACT"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_subtract),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!MULTIPLY"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_multiply),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!DIVIDE"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_divide),0));
            //comparison
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!GREATER"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_greater),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!LESSER"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_lesser),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!EQUIVALENT"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_equivalent),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!AND"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_and),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!OR"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_or),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!EQUIVALENT_OR_GREATER"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_equivalent_or_greater),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!EQUIVALENT_OR_LESSER"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_equivalent_or_lesser),0));
            //experimental
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!SELECT"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_selection),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!SORT"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_sort),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!SIZE"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_size),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!RECURSIVE_SIZE"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_recursive_size),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!PRINT"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_print),0));
            ////RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("!RECOMBINATOR"),0),Arena_Allocator.Borrow(FunctionValue_st(Luna::Shell::Functions::F_recombinator),0));
            //constants
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("TRUE"),0),Arena_Allocator.Borrow(BoolValue_st(1),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("FALSE"),0),Arena_Allocator.Borrow(BoolValue_st(-1),0));
            RootEnviroment->Set(Arena_Allocator.Borrow(SymbolValue_st("NULL"),0),Arena_Allocator.Borrow(BoolValue_st(0),0));
        }
    };
};