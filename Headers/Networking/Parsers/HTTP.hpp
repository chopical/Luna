#ifndef __LUNA_NETWORKING_PARSERS_HTTP__
#define __LUNA_NETWORKING_PARSERS_HTTP__
#include <string>
#include <deque>
namespace Luna{
	namespace Networking{
		namespace Parsers{
			namespace HTTP{
				struct Propertie_st{
					std::string Tag;
					std::string Value;
				};
				struct Header_st{
					std::string Type;
					std::string Directory;
					std::string Connection;
					std::string Cache_Control;
					std::string DNT;
					std::string Upgrade_Insecure_Requests;
					std::string User_Agent;
					std::string Accept;
					std::string Sec_Fetch_Site;
					std::string Sec_Fetch_Mode;
					std::string Sec_Fetch_User;
					std::string Sec_Fetch_Dest;
					std::string Accept_Encoding;
					std::string Accept_Language;
					std::string Cookie;
					std::deque<Propertie_st> Others;
					inline std::string ToString(){
						//Header Print.
						std::string Result=std::string("Type:")+Type+std::string("\n");
						Result+=std::string("Directory: ")+Directory+std::string("\n");
						Result+=std::string("Connection: ")+Connection+std::string("\n");
						Result+=std::string("Cache_Control: ")+Cache_Control+std::string("\n");
						Result+=std::string("DNT: ")+DNT+std::string("\n");
						Result+=std::string("Upgrade_insecure_Requests: ")+Upgrade_Insecure_Requests+std::string("\n");
						Result+=std::string("User_Agent: ")+User_Agent+std::string("\n");
						Result+=std::string("Accept: ")+Accept+std::string("\n");
						Result+=std::string("Sec_Fetch_Site: ")+Sec_Fetch_Site+std::string("\n");
						Result+=std::string("Sec_Fetch_Mode: ")+Sec_Fetch_Mode+std::string("\n");
						Result+=std::string("Sec_Fetch_User: ")+Sec_Fetch_User+std::string("\n");
						Result+=std::string("Sec_Fetch_Dest: ")+Sec_Fetch_Dest+std::string("\n");
						Result+=std::string("Accept_Encoding: ")+Accept_Encoding+std::string("\n");
						Result+=std::string("Accept_Language: ")+Accept_Language+std::string("\n");
						Result+=std::string("Cookie: ")+Cookie+std::string("\n");
						for(size_t i=0;i<Others.size();i++){
							Result+=std::string("Unimplemented: [")+Others[i].Tag+std::string("|")+Others[i].Value+std::string("]\n");
						}
						return Result;
					}
				};
				std::deque<Propertie_st> Tokenize(std::string PageSource);
				Header_st Parse(std::deque<Propertie_st> Tokens);
				std::string ResponseHTTP(std::string Content,int64_t ResponseCode=200, std::string Cookie=".");
			};
		};
	};
};
#endif
