#ifndef __LUNA_SHELL_TYPES__
#define __LUNA_SHELL_TYPES__
#include <string>
#include <deque>
#include <unordered_map>
#include <stdfloat>
#include <functional>

#include "Utilities/Arena_Allocator.hpp"

namespace Luna{
    namespace Shell{
        namespace ValueType_en{
            enum ValueType_en{
                Type_Null=0,
                Type_Error,
                Type_Symbol,
                Type_Integer,
                Type_Float,
                Type_Function,
                Type_List,
                Type_Vector,
                Type_Hashmap,
                Type_Bool
            };
            inline std::string Str(ValueType_en Type){
                switch(Type){
                    case ValueType_en::Type_Null:return "Null";
                    case ValueType_en::Type_Error:return "Error";
                    case ValueType_en::Type_Symbol:return "Symbol";
                    case ValueType_en::Type_Integer:return "Integer";
                    case ValueType_en::Type_Float:return "Float";
                    case ValueType_en::Type_Function:return "Function";
                    case ValueType_en::Type_List:return "List";
                    case ValueType_en::Type_Vector:return "Vector";
                    case ValueType_en::Type_Hashmap:return "Hashmap";
                    case ValueType_en::Type_Bool:return "Bool";
                }
                return "Null";
            }
        };
    };
    namespace Shell{
        struct SymbolValue_st;
        struct IntegerValue_st;
        struct FloatValue_st;
        struct FunctionValue_st;
        struct ErrorValue_st;
        struct ListValue_st;
        struct VectorValue_st;
        struct HashMapValue_st;
        struct BoolValue_st;
    };
    namespace Shell{
        struct Value_st{
            virtual std::string String() const = 0;
            virtual ValueType_en::ValueType_en Type() const = 0;
            virtual Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const = 0;
            SymbolValue_st* As_Symbol();
            IntegerValue_st* As_Integer();
            FloatValue_st* As_Float();
            FunctionValue_st* As_Function();
            ErrorValue_st* As_Error();
            ListValue_st* As_List();
            VectorValue_st* As_Vector();
            HashMapValue_st* As_HashMap();
            BoolValue_st* As_Bool();
        };
    };
    namespace Shell{
        struct Token_st{
            int64_t LineNumber;
            int64_t ColumnNumber;
            std::string Value;
        };
        typedef std::function<Value_st*(std::deque<Value_st*>,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)> FnPtr;
    };
    namespace Shell{
        struct SymbolValue_st:public Value_st{
            std::string m_symbol;
            SymbolValue_st(std::string symbol);
            std::string String() const override;
            ValueType_en::ValueType_en Type() const override;
            Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const override;
        };
        struct IntegerValue_st:public Value_st{
            int64_t m_number;
            IntegerValue_st(int64_t number);
            std::string String() const override;
            ValueType_en::ValueType_en Type() const override;
            Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const override;
        };
        struct BoolValue_st:public Value_st{
            int64_t m_number;
            BoolValue_st(int64_t number);
            std::string String() const override;
            ValueType_en::ValueType_en Type() const override;
            Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const override;
        };
        struct FloatValue_st:public Value_st{
            std::float64_t m_number;
            FloatValue_st(std::float64_t number);
            std::string String() const override;
            ValueType_en::ValueType_en Type() const override;
            Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const override;
        };
        struct FunctionValue_st:public Value_st{
            FnPtr m_fun;
            FunctionValue_st(FnPtr fun);
            std::string String() const override;
            ValueType_en::ValueType_en Type() const override;
            Value_st* Call(std::deque<Value_st*> args,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator);
            Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const override;
        };
        struct ErrorValue_st:public Value_st{
            std::string m_error;
            int64_t m_line,m_column;
            ErrorValue_st(std::string error,int64_t line,int64_t column);
            std::string String() const override;
            ValueType_en::ValueType_en Type() const override;
            Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const override;
        };
        struct ListValue_st:public Value_st{
            ListValue_st(){}
            std::deque<Value_st*> m_list;
            void Push_front(Value_st* Input);
            void Push_back(Value_st* Input);
            inline void Push(Value_st* Input){Push_back(Input);}
            Value_st* Pop_front();
            Value_st* Pop_back();
            inline Value_st* Pop(){return Pop_back();}
            Value_st* Front()const;
            Value_st* Back()const;
            size_t size()const;
            size_t recursive_size()const;
            std::string String() const override;
            ValueType_en::ValueType_en Type() const override;
            Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const override;
        };
        struct VectorValue_st:public Value_st{
            VectorValue_st(){}
            std::deque<Value_st*> m_vector;
            void Push_front(Value_st* Input);
            void Push_back(Value_st* Input);
            inline void Push(Value_st* Input){Push_back(Input);}
            Value_st* Pop_front();
            Value_st* Pop_back();
            inline Value_st* Pop(){return Pop_back();}
            Value_st* Front()const;
            Value_st* Back()const;
            size_t size()const;
            size_t recursive_size()const;
            std::string String() const override;
            ValueType_en::ValueType_en Type() const override;
            Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const override;
        };
        struct HashMapValue_st : public Value_st{
            HashMapValue_st(){}
            struct Hasher{
                size_t operator()(Value_st* V) const noexcept {
                    return std::hash<std::string>{}(V->String());
                }
            };
            struct Equator{
                constexpr bool operator()(const Value_st* AV,const Value_st* BV) const{
                    return AV->String() == BV->String();
                }
            };
            std::unordered_map<Value_st*,Value_st*,Hasher,Equator> m_map;
            void Set(Value_st* Key,Value_st* Value);
            Value_st* Get(Value_st* Key)const;
            size_t size()const;
            size_t recursive_size()const;
            std::string String() const override;
            ValueType_en::ValueType_en Type() const override;
            Value_st* Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator) const override;
        };
    };
};
#endif