#ifndef __LUNA_NETWORKING_SERVER__
#define __LUNA_NETWORKING_SERVER__
#include "Networking/Socket.hpp"
namespace Luna{
	namespace Networking{
		struct Server_st{
			Luna::Networking::SocketServer_st SocketServer;
			bool Running{true};
			Server_st();
			static void HandleClient(size_t LocalID,Luna::Networking::SocketServer_st* rSocketServer);
		};
	};
};
#endif
