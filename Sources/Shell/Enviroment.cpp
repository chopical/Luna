#include "Shell/Enviroment.hpp"
namespace Luna{
    namespace Shell{
		Enviroment_cl::Enviroment_cl(Enviroment_cl* Parent):m_Parent(Parent){}
		void Enviroment_cl::Set(SymbolValue_st* Key,Value_st* Value){
			m_Variables[Key] = Value;
		}
		Value_st* Enviroment_cl::Get(SymbolValue_st* Key){
			auto search = m_Variables.find(Key);
			if(search == m_Variables.end() && m_Parent == nullptr){return nullptr;}
			else if(search == m_Variables.end()){return m_Parent->Get(Key);}
			return search->second;
		}
		Enviroment_cl* Enviroment_cl::Find(SymbolValue_st* Key){
			auto search = m_Variables.find(Key);
			if(search == m_Variables.end() && m_Parent == nullptr){return nullptr;}
			else if(search == m_Variables.end()){return m_Parent->Find(Key);}
			return this;
		}
	};
};