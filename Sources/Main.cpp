#include "Neural_Networking/Neural_Network.hpp"
#include "Utilities/Random.hpp"
#include "Neural_Networking/Functions.hpp"
#include "Networking/Server.hpp"
#include "Utilities/Choas.hpp"
#include "Shell/Interface.hpp"
#include "External/linenoise.hpp"

#include <iostream>
#include <string>


int main(int ArgumentCount,char** ArgumentValues){
	Luna::Shell::Interface_cl Interface;
	
	const auto history_path="History.txt";
	linenoise::LoadHistory(history_path);
	while(true){
		std::string UserInput="";
		if(linenoise::Readline("User> ",UserInput)||!UserInput.size())break;
		//std::cout<<Interface.Repl(UserInput)<<std::endl;
		if(UserInput.size()){
			linenoise::AddHistory(UserInput.c_str());
			linenoise::SaveHistory(history_path);
		}
		Interface.Repl(UserInput);
		
	}
	return 0;
}