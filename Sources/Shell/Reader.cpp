#include "Shell/Reader.hpp"

namespace Luna{
    namespace Shell{
        Tokenizer_cl::Tokenizer_cl(const std::string input):m_input(input){}
        Token_st Tokenizer_cl::Next(){
            while(m_index<m_input.size()){
                switch(m_input[m_index]){
                    case ',':case ' ':case '\t':case '\n':{
                        m_index++;
                        m_column++; // +=? tabs ;(
                        if(m_input[m_index] == '\n'){
                            m_column=0;
                            m_line++;
                        }
                    }break;
                    case '~':{
                        int64_t start=m_index;
                        m_index++;
                        if(1+m_index<m_input.size() && m_input[1+m_index] == '@')m_index++;
                        int64_t length=m_index-start;
                        m_column+=length;
                        return {m_line,m_column-length,m_input.substr(start,length)};
                    }break;
                    case '(':case ')':case '{':case '}':case '[':case ']':case '`':case '\'':case '^':case '@':case '.':case '/':case '*':case '=':case '?':{
                        int64_t start=m_index;
                        m_index++;
                        int64_t length=m_index-start;
                        m_column+=length;
                        return {m_line,m_column-length,m_input.substr(start,length)};
                    }break;
                    case '+':case '-':{
                        int64_t start=m_index;
                        m_index++;
                        while(++m_index<m_input.size() && (m_input[m_index] == '0'
                                                         ||m_input[m_index] == '1'
                                                         ||m_input[m_index] == '2'
                                                         ||m_input[m_index] == '3'
                                                         ||m_input[m_index] == '4'
                                                         ||m_input[m_index] == '5'
                                                         ||m_input[m_index] == '6'
                                                         ||m_input[m_index] == '7'
                                                         ||m_input[m_index] == '8'
                                                         ||m_input[m_index] == '9'
                                                         )){}
                        int64_t length=m_index-start;
                        m_column+=length;
                        return {m_line,m_column-length,m_input.substr(start,length)};
                    }break;
                    case '"':{
                        int64_t start=m_index;
                        while(++m_index<m_input.size() && !(m_input[m_index]=='"'&&m_input[m_index-1]!='\\'));
                        if(m_index<m_input.size() && m_input[m_index]=='"')m_index++;
                        int64_t length=m_index-start;
                        m_column+=length;
                        return {m_line,m_column-length,m_input.substr(start,length)};
                    }break;
                    case ';':{
                        int64_t start=m_index;
                        while(++m_index<m_input.size() && !(m_input[m_index]=='\n'&&m_input[m_index-1]!='\\'));
                        int64_t length=m_index-start;
                        m_column+=length;
                        return {m_line,m_column-length,m_input.substr(start,length)};
                    }break;
                    case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':{
                        int64_t start=m_index;
                        while(++m_index<m_input.size() && (m_input[m_index] == '0'
                                                         ||m_input[m_index] == '1'
                                                         ||m_input[m_index] == '2'
                                                         ||m_input[m_index] == '3'
                                                         ||m_input[m_index] == '4'
                                                         ||m_input[m_index] == '5'
                                                         ||m_input[m_index] == '6'
                                                         ||m_input[m_index] == '7'
                                                         ||m_input[m_index] == '8'
                                                         ||m_input[m_index] == '9'
                                                         )){}
                        int64_t length=m_index-start;
                        m_column+=length;
                        return {m_line,m_column-length,m_input.substr(start,length)};
                    }break;
                    default:{
                        int64_t start=m_index;
                        while(++m_index<m_input.size() && !(m_input[m_index]=='"'
                                                          ||m_input[m_index]==','
                                                          ||m_input[m_index]==' '
                                                          ||m_input[m_index]=='\t'
                                                          ||m_input[m_index]=='\n'
                                                          ||m_input[m_index]=='('
                                                          ||m_input[m_index]==')'
                                                          ||m_input[m_index]=='{'
                                                          ||m_input[m_index]=='}'
                                                          ||m_input[m_index]=='['
                                                          ||m_input[m_index]==']'
                                                          ||m_input[m_index]=='`'
                                                          ||m_input[m_index]=='\''
                                                          ||m_input[m_index]=='^'
                                                          ||m_input[m_index]=='~'
                                                          ||m_input[m_index]=='@'
                                                          ||m_input[m_index]=='.'
                                                          ||m_input[m_index]=='+'
                                                          ||m_input[m_index]=='-'
                                                          ||m_input[m_index]=='/'
                                                          ||m_input[m_index]=='*'
                                                          ||m_input[m_index]=='='
                                                          ||m_input[m_index]=='?'
                                                          ||m_input[m_index]=='"'
                                                          ||m_input[m_index]==';'
                                                          ));
                        int64_t length=m_index-start;
                        m_column+=length;
                        return {m_line,m_column-length,m_input.substr(start,length)};
                    }break;
                }
            }
            return {-1,-1,""};
        }
        std::deque<Token_st> Tokenizer_cl::Tokenize(std::string input){
            Tokenizer_cl Tokenizer(input);
            std::deque<Token_st> Output;
            Token_st token=Tokenizer.Next();
            while(token.Value.size()){
                Output.push_back(token);
                token=Tokenizer.Next();
            }
            return Output;
        }
        Reader_cl::Reader_cl(std::deque<Token_st> input):m_input(input){}
        Token_st Reader_cl::Peek(int64_t offset){
            if(m_index+offset<m_input.size() && m_index+offset>-1)return m_input[m_index+offset];
            return {-1,-1,""};
        }
        Token_st Reader_cl::Next(){
            m_index++;
            return Peek(-1);
        }
    };
    namespace Shell{
        Value_st* Read_str(std::string Input){
            std::deque<Token_st> tokens = Tokenizer_cl::Tokenize(Input);
            Reader_cl Reader(tokens);
            return Read_form(Reader);
        }
        Value_st* Read_form(Reader_cl& Reader){
            Token_st token = Reader.Peek();
            if(token.Value.size() == 0){
                Token_st PreviousToken = Reader.Peek(-1);
                return new ErrorValue_st("Found unexpected end of tokens during parse.",PreviousToken.LineNumber,PreviousToken.ColumnNumber+PreviousToken.Value.size());
            }
            switch(token.Value[0]){
                case '(':return Read_list(Reader);
                case '[':return Read_vector(Reader);
                case '{':return Read_hashmap(Reader);
                case '\'':return Read_quote(Reader);
                case '`':return Read_quote(Reader);
                case '~':return Read_quote(Reader);
                case '^':return Read_meta(Reader);
                case '@':return Read_quote(Reader);
                case '+':case '-':case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':return Read_number(Reader);
                default:return Read_atom(Reader);
            }
            return nullptr;
        }
        Value_st* Read_list(Reader_cl& Reader){
            Reader.Next(); //consume openning '('
            ListValue_st* List = new ListValue_st;
            Token_st token=Reader.Peek();
            while(token.Value.size() && token.Value[0] != ')'){
                auto X = Read_form(Reader);
                List->Push(X);
                token=Reader.Peek();
            }
            if(token.Value[0] == ')'){
                Reader.Next();
            }else{
                Token_st PreviousToken = Reader.Peek(-1);
                List->Push(new ErrorValue_st("Found unterminated list during parse.",PreviousToken.LineNumber,PreviousToken.ColumnNumber+PreviousToken.Value.size()));
            }
            return List;
        }
        Value_st* Read_vector(Reader_cl& Reader){
            Reader.Next(); //consume openning '['
            VectorValue_st* Vector = new VectorValue_st;
            Token_st token=Reader.Peek();
            while(token.Value.size() && token.Value[0] != ']'){
                auto X = Read_form(Reader);
                Vector->Push(X);
                token=Reader.Peek();
            }
            if(token.Value[0] == ']'){
                Reader.Next();
            }else{
                Token_st PreviousToken = Reader.Peek(-1);
                Vector->Push(new ErrorValue_st("Found unterminated vector during parse.",PreviousToken.LineNumber,PreviousToken.ColumnNumber+PreviousToken.Value.size()));
            }
            return Vector;
        }
        Value_st* Read_hashmap(Reader_cl& Reader){
            Reader.Next(); //consume openning '{'
            HashMapValue_st* Hashmap = new HashMapValue_st;
            Token_st token=Reader.Peek();
            while(token.Value.size() != 0 && token.Value[0] != '}'){
                //?
                auto Key = Read_form(Reader);
                token=Reader.Peek();
                if(token.Value.size() == 0 || token.Value[0] == '}'){
                    Token_st PreviousToken = Reader.Peek(-1);
                    return new ErrorValue_st("Found incomplete key-pair in hashmap during parse.",PreviousToken.LineNumber,PreviousToken.ColumnNumber+PreviousToken.Value.size());
                }
                auto Value = Read_form(Reader);
                Hashmap->Set(Key,Value);
                token=Reader.Peek();
            }
            if(token.Value[0] == '}'){
                Reader.Next();
            }else{
                Token_st PreviousToken = Reader.Peek(-1);
                Hashmap->Set(new SymbolValue_st("ERROR"),new ErrorValue_st("Found unterminated hashmap during parse.",PreviousToken.LineNumber,PreviousToken.ColumnNumber+PreviousToken.Value.size()));
            }
            return Hashmap;
        }
        Value_st* Read_quote(Reader_cl& Reader){
            ListValue_st* Quote = new ListValue_st;
            Token_st QuoteType = Reader.Next();
            switch(QuoteType.Value[0]){
                case '\'':{
                    Quote->Push(new SymbolValue_st("quote"));
                    Quote->Push(Read_form(Reader));
                    return Quote;
                }break;
                case '`':{
                    Quote->Push(new SymbolValue_st("quasiquote"));
                    Quote->Push(Read_form(Reader));
                    return Quote;
                }break;
                case '~':{
                    if(QuoteType.Value.size() > 1 && QuoteType.Value[1] == '@'){
                        Quote->Push(new SymbolValue_st("splice-unquote"));
                        Quote->Push(Read_form(Reader));
                        return Quote;
                    }
                    Quote->Push(new SymbolValue_st("unquote"));
                    Quote->Push(Read_form(Reader));
                    return Quote;
                }break;
                case '@':{
                    Quote->Push(new SymbolValue_st("deref"));
                    Quote->Push(Read_form(Reader));
                    return Quote;
                }break;
                default:{
                    Quote->Push(new ErrorValue_st("Found unimplemented quote during parse.",QuoteType.LineNumber,QuoteType.ColumnNumber));
                    return Quote;
                }break;
            }
            return new ErrorValue_st("Something happened during parsing a quote *shrugs*, good luck.",QuoteType.LineNumber,QuoteType.ColumnNumber);
        }
        Value_st* Read_meta(Reader_cl& Reader){
            Token_st MetaStart = Reader.Next(); // consume the '^'
            ListValue_st* WithMeta = new ListValue_st;
            
            auto meta = Read_form(Reader);
            if(Reader.Peek().Value.size() == 0){
                return new ErrorValue_st("Found unexpected end of tokens during parse.",Reader.Peek(-1).LineNumber,Reader.Peek(-1).ColumnNumber+Reader.Peek(-1).Value.size());
            }
            auto value = Read_form(Reader);
            //TODO: check to see if meta is a hashmap and value is a vector.
            if(meta->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_Hashmap || value->Type() != Luna::Shell::ValueType_en::ValueType_en::Type_List){
                return new ErrorValue_st("Found malformed with-meta during parse.",MetaStart.LineNumber,MetaStart.ColumnNumber);
            }
            WithMeta->Push(new SymbolValue_st("with-meta"));
            WithMeta->Push(value);
            WithMeta->Push(meta);
            return WithMeta;
        }
        Value_st* Read_number(Reader_cl& Reader){
            //Integer ('+'||'-'||)(N)(!'.')&&(!N)
            //Float ('+'||'-'||)(N)('.')(N)
            bool Sign=true;
            bool Signed=false;
            while(Reader.Peek().Value == "+"||Reader.Peek().Value == "-"){
                if(Reader.Peek().Value == "+")Sign=true;
                if(Reader.Peek().Value == "-")Sign=!Sign;
                Signed=true;
                Reader.Next();
            }
            Token_st N1 = Reader.Next();
            if(Reader.Peek().Value != "." || !(Reader.Peek(1).Value[0] == '0'
                                             ||Reader.Peek(1).Value[0] == '1'
                                             ||Reader.Peek(1).Value[0] == '2'
                                             ||Reader.Peek(1).Value[0] == '3'
                                             ||Reader.Peek(1).Value[0] == '4'
                                             ||Reader.Peek(1).Value[0] == '5'
                                             ||Reader.Peek(1).Value[0] == '6'
                                             ||Reader.Peek(1).Value[0] == '7'
                                             ||Reader.Peek(1).Value[0] == '8'
                                             ||Reader.Peek(1).Value[0] == '9'
                                             )){
                                                if(!Signed)return new IntegerValue_st(std::stoll(N1.Value));
                                                if(Sign)return new IntegerValue_st(std::stoll(N1.Value));
                                                return new IntegerValue_st(-std::stoll(N1.Value));
                                            }
            Token_st Dot = Reader.Next();
            Token_st N2 = Reader.Next();
            std::string Nt = N1.Value+Dot.Value+N2.Value;
            if(!Signed)return new FloatValue_st(std::stof(Nt));
            if(Sign)return new FloatValue_st(std::stof(Nt));
            return new FloatValue_st(-std::stof(Nt));
        }
        Value_st* Read_atom(Reader_cl& Reader){
            return new SymbolValue_st(Reader.Next().Value);
        }
    };
};