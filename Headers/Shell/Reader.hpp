#ifndef __LUNA_SHELL_READER__
#define __LUNA_SHELL_READER__
#include "Shell/Types.hpp"
#include <string>
#include <deque>
namespace Luna{
    namespace Shell{
        class Tokenizer_cl{
            const std::string m_input;
            int64_t m_index{0};
            int64_t m_line{0};
            int64_t m_column{0};
            public:
                Tokenizer_cl(const std::string input);
                Token_st Next();
                static std::deque<Token_st> Tokenize(std::string input);
        };
        class Reader_cl{
            std::deque<Token_st> m_input;
            int64_t m_index{0};
            public:
            Reader_cl(std::deque<Token_st> input);
            Token_st Peek(int64_t offset=0);
            Token_st Next();
        };
    };
    namespace Shell{
        Value_st* Read_str(std::string Input);
        Value_st* Read_form(Reader_cl& Reader);
        Value_st* Read_list(Reader_cl& Reader);
        Value_st* Read_vector(Reader_cl& Reader);
        Value_st* Read_hashmap(Reader_cl& Reader);
        Value_st* Read_quote(Reader_cl& Reader);
        Value_st* Read_meta(Reader_cl& Reader);
        Value_st* Read_number(Reader_cl& Reader);
        Value_st* Read_atom(Reader_cl& Reader);
    };
};
#endif