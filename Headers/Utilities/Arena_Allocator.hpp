#ifndef __LUNA_UTILITIES_ARENA_ALLOCATOR__
#define __LUNA_UTILITIES_ARENA_ALLOCATOR__

#include <deque>
#include <cstdint>
#include <cstddef>
#include <cassert>

namespace Luna{
    namespace Utilities{
		class Arena_Allocator_cl{
			struct SegmentData_st{
				uint8_t* ptr;
				size_t size;
				size_t Priority;
			};
			uint8_t* Arena{nullptr};
			std::deque<SegmentData_st> AllocatedSegments{};
			size_t ArenaSize{0};
			size_t MaxPriority{0};
			public:
				Arena_Allocator_cl(size_t ArenaSize);
				size_t TotalUsed();
				size_t TotalFree();
				uint8_t* Seek(uint8_t* Addr);
				uint8_t* Fit(size_t QueueSize);
				uint8_t* BorrowRaw(size_t QueueSize,size_t Priority);
				void ReturnRaw(uint8_t* Addr);
				void Yeet(size_t Priority=-1);

				template<typename T1>
				inline T1* Borrow(const T1& init,size_t Priority=-1){
					T1* Thing = (T1*)BorrowRaw(sizeof(T1),Priority);
					//Foo* foo = new (your_memory_address_here) Foo ();
					Thing = new (Thing) T1 (init);
					assert(Thing != nullptr);
					return Thing;
				}
				template<typename T2>
				inline void Return(T2* Addr){
					ReturnRaw((uint8_t*)Addr);
				}
		};
	};
};
#endif