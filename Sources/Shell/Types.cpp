#include "Shell/Types.hpp"
#include <cassert>

namespace Luna{
    namespace Shell{
        //Value_st
        SymbolValue_st* Value_st::As_Symbol(){
            assert(Type() == ValueType_en::ValueType_en::Type_Symbol);
            return static_cast<SymbolValue_st*>(this);
        }
        IntegerValue_st* Value_st::As_Integer(){
            assert(Type() == ValueType_en::ValueType_en::Type_Integer);
            return static_cast<IntegerValue_st*>(this);
        }
        FloatValue_st* Value_st::As_Float(){
            assert(Type() == ValueType_en::ValueType_en::Type_Float);
            return static_cast<FloatValue_st*>(this);
        }
        FunctionValue_st* Value_st::As_Function(){
            assert(Type() == ValueType_en::ValueType_en::Type_Function);
            return static_cast<FunctionValue_st*>(this);
        }
        ErrorValue_st* Value_st::As_Error(){
            assert(Type() == ValueType_en::ValueType_en::Type_Error);
            return static_cast<ErrorValue_st*>(this);
        }
        ListValue_st* Value_st::As_List(){
            assert(Type() == ValueType_en::ValueType_en::Type_List);
            return static_cast<ListValue_st*>(this);
        }
        VectorValue_st* Value_st::As_Vector(){
            assert(Type() == ValueType_en::ValueType_en::Type_Vector);
            return static_cast<VectorValue_st*>(this);
        }
        HashMapValue_st* Value_st::As_HashMap(){
            assert(Type() == ValueType_en::ValueType_en::Type_Hashmap);
            return static_cast<HashMapValue_st*>(this);
        }
        BoolValue_st* Value_st::As_Bool(){
            assert(Type() == ValueType_en::ValueType_en::Type_Bool);
            return static_cast<BoolValue_st*>(this);
        }
        //SymbolValue_st
        SymbolValue_st::SymbolValue_st(std::string symbol):m_symbol(symbol){}
        std::string SymbolValue_st::String() const{
            return m_symbol;
        }
        ValueType_en::ValueType_en SymbolValue_st::Type() const{
            return ValueType_en::ValueType_en::Type_Symbol;
        }
        Value_st* SymbolValue_st::Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)const{
            return Arena_Allocator.Borrow(SymbolValue_st(m_symbol));
        }
        //IntegerValue_st
        IntegerValue_st::IntegerValue_st(int64_t number):m_number(number){}
        std::string IntegerValue_st::String() const{
            return std::to_string(m_number);
        }
        ValueType_en::ValueType_en IntegerValue_st::Type() const{
            return ValueType_en::ValueType_en::Type_Integer;
        }
        Value_st* IntegerValue_st::Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)const{
            return Arena_Allocator.Borrow(IntegerValue_st(m_number));
        }
        //BoolValue_st
        BoolValue_st::BoolValue_st(int64_t number):m_number(number){}
        std::string BoolValue_st::String() const{
            if(m_number > 0)return "TRUE";
            if(m_number < 0)return "FALSE";
            return "NULL";
        }
        ValueType_en::ValueType_en BoolValue_st::Type()const{
            return ValueType_en::ValueType_en::Type_Bool;
        }
        Value_st* BoolValue_st::Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)const{
            return Arena_Allocator.Borrow(BoolValue_st(m_number));
        }
        //FloatValue_st
        FloatValue_st::FloatValue_st(std::float64_t number):m_number(number){}
        std::string FloatValue_st::String() const{
            return std::to_string(m_number);
        }
        ValueType_en::ValueType_en FloatValue_st::Type() const{
            return ValueType_en::ValueType_en::Type_Float;
        }
        Value_st* FloatValue_st::Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)const{
            return Arena_Allocator.Borrow(FloatValue_st(m_number));
        }
        //FunctionValue_st
        FunctionValue_st::FunctionValue_st(FnPtr fun):m_fun(fun){}
        std::string FunctionValue_st::String() const{
            return "<FN>";
        }
        ValueType_en::ValueType_en FunctionValue_st::Type() const{
            return ValueType_en::ValueType_en::Type_Function;
        }
        Value_st* FunctionValue_st::Call(std::deque<Value_st*> args,Luna::Utilities::Arena_Allocator_cl& Arena_Allocator){
            return m_fun(args,Arena_Allocator);
        }
        Value_st* FunctionValue_st::Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)const{
            return Arena_Allocator.Borrow(FunctionValue_st(m_fun));
        }
        //ErrorValue_st
        ErrorValue_st::ErrorValue_st(std::string error,int64_t line,int64_t column):m_error(error),m_line(line),m_column(column){}
        std::string ErrorValue_st::String() const{
            return std::string("ERROR(")+std::to_string(m_line)+std::string(",")+std::to_string(m_column)+std::string("):")+m_error;
        }
        ValueType_en::ValueType_en ErrorValue_st::Type() const{
            return ValueType_en::ValueType_en::Type_Error;
        }
        Value_st* ErrorValue_st::Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)const{
            return Arena_Allocator.Borrow(ErrorValue_st(m_error,m_line,m_column));
        }
        //ListValue_st
        void ListValue_st::Push_front(Value_st* Input){
            m_list.push_front(Input);
        }
        void ListValue_st::Push_back(Value_st* Input){
            m_list.push_back(Input);
        }
        Value_st* ListValue_st::Pop_front(){
            Value_st* r = m_list.front();
            m_list.pop_front();
            return r;
        }
        Value_st* ListValue_st::Pop_back(){
            Value_st* r = m_list.back();
            m_list.pop_back();
            return r;
        }
        Value_st* ListValue_st::Front()const{
            Value_st* r = m_list.front();
            return r;
        }
        Value_st* ListValue_st::Back()const{
            Value_st* r = m_list.back();
            return r;
        }
        size_t ListValue_st::size()const{
            return m_list.size();
        }
        size_t ListValue_st::recursive_size()const{
            size_t Sizea=0;
            for(auto element : m_list){
				switch(element->Type()){
					case Luna::Shell::ValueType_en::ValueType_en::Type_List:{
						Sizea+=element->As_List()->size();
					}break;
					case Luna::Shell::ValueType_en::ValueType_en::Type_Vector:{
						Sizea+=element->As_Vector()->size();
					}break;
					case Luna::Shell::ValueType_en::ValueType_en::Type_Hashmap:{
						Sizea+=element->As_HashMap()->size();
					}break;
					default:{
						Sizea++;
					}break;
				}
			}
            return Sizea;
        }
        std::string ListValue_st::String() const{
            std::string r="(";
            for(auto element : m_list){
                r+=element->String();
                r+=" ";
            }
            if(r.at(r.size()-1) != ' '){r+=")";}else{r.at(r.size()-1) = ')';}
            return r;
        }
        ValueType_en::ValueType_en ListValue_st::Type() const{
            return ValueType_en::ValueType_en::Type_List;
        }
        Value_st* ListValue_st::Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)const{
            ListValue_st* DList = Arena_Allocator.Borrow(ListValue_st());
            for(auto element : m_list){
                DList->Push_back(element->Duplicate(Arena_Allocator));
            }
            return DList;
        }
        //VectorValue_st
        void VectorValue_st::Push_front(Value_st* Input){
            m_vector.push_front(Input);
        }
        void VectorValue_st::Push_back(Value_st* Input){
            m_vector.push_back(Input);
        }
        Value_st* VectorValue_st::Pop_front(){
            Value_st* r = m_vector.front();
            m_vector.pop_front();
            return r;
        }
        Value_st* VectorValue_st::Pop_back(){
            Value_st* r = m_vector.back();
            m_vector.pop_back();
            return r;
        }
        Value_st* VectorValue_st::Front()const{
            Value_st* r = m_vector.front();
            return r;
        }
        Value_st* VectorValue_st::Back()const{
            Value_st* r = m_vector.back();
            return r;
        }
        size_t VectorValue_st::size()const{
            return m_vector.size();
        }
        size_t VectorValue_st::recursive_size()const{
            size_t Sizea=0;
            for(auto element : m_vector){
				switch(element->Type()){
					case Luna::Shell::ValueType_en::ValueType_en::Type_List:{
						Sizea+=element->As_List()->size();
					}break;
					case Luna::Shell::ValueType_en::ValueType_en::Type_Vector:{
						Sizea+=element->As_Vector()->size();
					}break;
					case Luna::Shell::ValueType_en::ValueType_en::Type_Hashmap:{
						Sizea+=element->As_HashMap()->size();
					}break;
					default:{
						Sizea++;
					}break;
				}
			}
            return Sizea;
        }
        std::string VectorValue_st::String() const{
            std::string r="[";
            for(size_t i=0;i<m_vector.size();i++){
                r+=m_vector[i]->String();
                if(1+i<m_vector.size())r+=" ";
            }
            r+="]";
            return r;
        }
        ValueType_en::ValueType_en VectorValue_st::Type() const{
            return ValueType_en::ValueType_en::Type_Vector;
        }
        Value_st* VectorValue_st::Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)const{
            VectorValue_st* DVector = Arena_Allocator.Borrow(VectorValue_st());
            for(auto element : m_vector){
                DVector->Push_back(element->Duplicate(Arena_Allocator));
            }
            return DVector;
        }
        //HashMapValue_st
        void HashMapValue_st::Set(Value_st* Key,Value_st* Value){
            m_map[Key] = Value;
        }
        Value_st* HashMapValue_st::Get(Value_st* Key)const{
            auto search = m_map.find(Key);
            if(search != m_map.end()){
                return search->second;
            }
            return nullptr;
        }
        size_t HashMapValue_st::size()const{
            return m_map.size();
        }
        size_t HashMapValue_st::recursive_size()const{
            size_t Sizea=0;
            for(auto element : m_map){
				switch(element.second->Type()){
					case Luna::Shell::ValueType_en::ValueType_en::Type_List:{
						Sizea+=element.second->As_List()->size();
					}break;
					case Luna::Shell::ValueType_en::ValueType_en::Type_Vector:{
						Sizea+=element.second->As_Vector()->size();
					}break;
					case Luna::Shell::ValueType_en::ValueType_en::Type_Hashmap:{
						Sizea+=element.second->As_HashMap()->size();
					}break;
					default:{
						Sizea++;
					}break;
				}
			}
            return Sizea;
        }
        std::string HashMapValue_st::String() const{
            std::string r="{";
            for(auto pair : m_map){
                r+=pair.first->String();
                r+=" ";
                r+=pair.second->String();
                r+=" ";
            }

            if(r[r.size()-1] == ' '){r[r.size()-1]='}';}else{r+="}";}
            return r;
        }
        ValueType_en::ValueType_en HashMapValue_st::Type() const{
            return ValueType_en::ValueType_en::Type_Hashmap;
        }
        Value_st* HashMapValue_st::Duplicate(Luna::Utilities::Arena_Allocator_cl& Arena_Allocator)const{
            HashMapValue_st* DMap = Arena_Allocator.Borrow(HashMapValue_st());
            for(auto element : m_map){
                DMap->Set(element.first->Duplicate(Arena_Allocator),element.second->Duplicate(Arena_Allocator));
            }
            return DMap;
        }
    };
};