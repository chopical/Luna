Name=Luna
Executable = ${Name}.exe
CC = g++
INCDIR = Headers
CFLAGS = -std=c++23 -I$(INCDIR)
SRCDIR := Sources
BUILDDIR = Build
BOOTEFI := $(GNUEFI)/x86_64/bootloader/main.efi


rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))
SRC = $(call rwildcard,$(SRCDIR),*.cpp)
OBJS = $(patsubst $(SRCDIR)/%.cpp, $(BUILDDIR)/%.o, $(SRC))

$(BUILDDIR)/$(Executable):$(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS)

$(BUILDDIR)/%.o:$(SRCDIR)/%.cpp
	@echo ##$^ -> $@
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -c $^ -o $@


.PHONY: clean compile recompile run debug zip default commit push

clean:
	rm -frv $(BUILDDIR)/*

compile: $(BUILDDIR)/$(Executable)

recompile: clean compile

run: compile
	./$(BUILDDIR)/$(Executable)

debug: compile
	gdb -ex run ./$(BUILDDIR)/$(Executable)

zip: clean
	zip -ru0 ../${Name}.zip .

commit: compile clean
	git add .
	git commit -am "$(shell date)"

push: commit
	git push

default:
	rm -frv ${INCDIR} ${SRCDIR} ${BUILDDIR}
	mkdir -pv ${INCDIR} ${SRCDIR} ${BUILDDIR}
	echo "int main(int ArgumentCount,char** ArgumentValues){return 0;}">>${SRCDIR}/Main.cpp
