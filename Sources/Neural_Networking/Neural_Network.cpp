#include "Neural_Networking/Neural_Network.hpp"
#include <iostream>
#include <cmath>
namespace Luna{
    namespace Neural_Networking{
        Vector_nt Neural_Network_cl::GenerateRandom(size_t c){
            Vector_nt result = {};
            for(size_t i=0;i<c;i++)result.push_back(0.5-m_rand.Generate_Float());
            return result;
        }
        Neural_Network_cl::Neural_Network_cl(size_nt Topology,std::function<std::float64_t(std::float64_t,std::float64_t)>Activate,std::function<std::float64_t(std::float64_t,std::float64_t)>Derive,size_t seed)
        :m_rand(seed,999999),
         m_Activate(Activate),
         m_Derive(Derive){
            m_Topology=Topology;
            for(size_t iY=0;iY<Topology.size();iY++){
                Vector_nt NewValueLayer = {};
                Vector_nt NewWeightLayer = {};
                Vector_nt NewBiasLayer = {};
                for(size_t iX=0;iX<Topology[iY];iX++){
                    NewValueLayer.push_back(0.0);
                    NewWeightLayer.push_back(GenerateRandom(1)[0]);
                    NewBiasLayer.push_back(GenerateRandom(1)[0]);
                }
                m_Values.push_back(NewValueLayer);
                m_Weights.push_back(NewWeightLayer);
                m_DeltaWeights.push_back(NewValueLayer);
                m_Biases.push_back(NewBiasLayer);
            }
        }
        neuron_nt Neural_Network_cl::Get(size_t Y,size_t X){
            //Weight,Bias,Value
            if(Y >= m_Topology.size()){
                std::cerr<<"ERROR: accessing out of bound row "<<Y<<"/"<<m_Topology.size()-1<<std::endl;
                exit(-1);
            }
            if(X >= m_Topology[Y]){
                std::cerr<<"ERROR: accessing out of bound column "<<X<<"/"<<m_Topology[Y]-1<<std::endl;
                exit(-1);
            }
            return neuron_nt{m_Weights[Y][X],m_Biases[Y][X],m_Values[Y][X]};
        }
        void Neural_Network_cl::FeedLoop(Vector_nt Input){
            if(Input.size() != m_Topology[0]){
                std::cerr<<"ERROR: Incorrect sized input, expected:"<<m_Topology[0]<<" received:"<<Input.size()<<std::endl;
            }
            FeedForward(Input);
            FeedBackward(Back());
        }
        void Neural_Network_cl::FeedForward(Vector_nt Input){
            if(Input.size() != m_Topology[0]){
                std::cerr<<"ERROR: Incorrect sized input, expected:"<<m_Topology[0]<<" received:"<<Input.size()<<std::endl;
            }
            for(size_t i=0;i<m_Topology[0];i++){m_Values[0][i]=Input[i];}
            for(size_t iY=1;iY<m_Topology.size();iY++){
                for(size_t iX=0;iX<m_Topology[iY];iX++){
                    std::float64_t V=0.0;
                    for(size_t iZ=0;iZ<m_Topology[iY-1];iZ++){
                        V+=(Get(iY-1,iZ).Value*Get(iY-1,iZ).Weight);
                    }
                    V+=Get(iY,iX).Bias;
                    V = m_Activate(V,1.0);
                    Get(iY,iX).Value=V;
                }
            }
        }
        void Neural_Network_cl::FeedBackward(Vector_nt Input){
            if(Input.size() != m_Topology.back()){
                std::cerr<<"ERROR: Incorrect sized input, expected:"<<m_Topology.back()<<" received:"<<Input.size()<<std::endl;
            }
            for(size_t i=0;i<m_Topology.back();i++){m_Values.back()[i]=Input[i];}
            for(int64_t iY=m_Topology.back()-2;iY>0;iY--){
                for(int64_t iX=m_Topology[iY]-1;iX>=0;iX--){
                    std::float64_t V=0.0;
                    for(int64_t iZ=0;iZ<m_Topology[iY+1];iZ++){
                        V+=(Get(iY+1,iZ).Value*Get(iY+1,iZ).Weight);
                    }
                    V+=Get(iY,iX).Bias;
                    V = m_Activate(V,1.0);
                    Get(iY,iX).Value=V;
                }
            }
        }
        void Neural_Network_cl::BackProp(Vector_nt Input){
            if(Input.size() != m_Topology.back()){
                std::cerr<<"ERROR: Incorrect sized input, expected:"<<m_Topology.back()<<" received:"<<Input.size()<<std::endl;
            }
            // calculate net error, Root Mean Square Error.
            std::float64_t error = 0.0;
            for(size_t iX=0;iX<m_Topology.back();iX++){
                std::float64_t delta = Input[iX] - m_Values.back()[iX];
                error+=delta*delta;
            }
            error/=m_Topology.back();
            error=sqrt(error);
            // setup gradient matrix.
            Matrix_nt gradients={};
            for(size_t iY=0;iY<m_Topology.size();iY++){
                Vector_nt gradientLayer = {};
                for(size_t iX=0;iX<m_Topology[iY];iX++){
                    gradientLayer.push_back(0.00);
                }
                gradients.push_back(gradientLayer);
            }
            // calculate gradients for output.
            for(size_t iX=0;iX<m_Topology.back();iX++){
                //m_value.back()[iX].calcOutputGradients(Input[iX]);
                std::float64_t delta=Input[iX] - m_Values.back()[iX];
                gradients.back()[iX]=delta * m_Derive(m_Values.back()[iX],1.0);
            }
            // calculate gradients for hidden.
            for(size_t iY=m_Topology.size()-2;iY;iY--){
                for(size_t iX=0;iX<m_Topology[iY];iX++){
                    //m_value[iY][iX].calcHiddenGradients(m_value[iY+1][iX]);
                    std::float64_t delta=0.00;
                    for(size_t iZ=0;iZ<m_Topology[iY+1];iZ++){
                        delta+=m_Weights[iY+1][iZ]*gradients[iY+1][iZ];
                    }
                    gradients[iY][iX]=delta * m_Derive(m_Values[iY][iX],1.0);
                }
            }
            //update weights
            std::float64_t eta = 0.5; // learning rate;
            std::float64_t alpha = 0.1; // fraction to take from old weight;
            for(size_t iY=m_Topology.size()-1;iY;iY--){
                for(size_t iX=0;iX<m_Topology[iY];iX++){
                    //m_value[iY][iX].updateWeights(m_value[iY-1][iX])
                    for(size_t iZ=0;iZ<m_Topology[iY-1];iZ++){
                        std::float64_t oldDeltaWeight=m_DeltaWeights[iY-1][iZ];
                        std::float64_t newDeltaWeight = \
                            // Individual input, magnified by the gradient and train rate
                            eta \
                            * m_Values[iY-1][iZ] \
                            * gradients[iY][iX] \
                            // also add momentum = fraction of the previous delta weight
                            + alpha \
                            * oldDeltaWeight;
                        m_DeltaWeights[iY-1][iZ] = newDeltaWeight;
                        m_Weights[iY-1][iZ] += newDeltaWeight;
                    }
                }
            }
        }
        Vector_nt Neural_Network_cl::Back(){
            return m_Values.back();
        }
        Vector_nt Neural_Network_cl::Front(){
            return m_Values[0];
        }
        std::string Neural_Network_cl::Str_Values(){
            std::string result="Values->";
            for(size_t iY=0;iY<m_Values.size();iY++){
                result+="\n\t";
                for(size_t iX=0;iX<m_Values[iY].size();iX++){
                    result+=std::to_string(m_Values[iY][iX]);
                    if(1+iX<m_Values[iY].size())result+=",";
                }
            }
            result+="\n<-Values";
            return result;
        }
        std::string Neural_Network_cl::Str_Weights(){
            std::string result="Weights->";
            for(size_t iY=0;iY<m_Weights.size();iY++){
                result+="\n\t";
                for(size_t iX=0;iX<m_Weights[iY].size();iX++){
                    result+=std::to_string(m_Weights[iY][iX]);
                    if(1+iX<m_Weights[iY].size())result+=",";
                }
            }
            result+="\n<-Weights";
            return result;
        }
        std::string Neural_Network_cl::Str_Biases(){
            std::string result="Biases->";
            for(size_t iY=0;iY<m_Biases.size();iY++){
                result+="\n\t";
                for(size_t iX=0;iX<m_Biases[iY].size();iX++){
                    result+=std::to_string(m_Biases[iY][iX]);
                    if(1+iX<m_Biases[iY].size())result+=",";
                }
            }
            result+="\n<-Biases";
            return result;
        }
        std::string Neural_Network_cl::Str(){
            std::string result="Network:{\n";
            result+=Str_Weights();
            result+="\n";
            result+=Str_Biases();
            result+="\n";
            result+=Str_Values();
            result+="\n}";
            return result;
        }
    };
};