#include "Networking/Server.hpp"
#include "Networking/Parsers/HTTP.hpp"
#include <iostream>
#include <thread>



namespace Luna{
	namespace Networking{
		Server_st::Server_st(){
			while(Running){
				size_t LocalID=SocketServer.Accept();
				std::thread(HandleClient,LocalID,&SocketServer).detach();

			}
		}
		void Server_st::HandleClient(size_t LocalID,Luna::Networking::SocketServer_st* rSocketServer){
			std::cout<<"#Client connected: ["<<LocalID<<","<<rSocketServer->GetIP(LocalID)<<"]"<<std::endl;
			std::string Data=rSocketServer->Receive(LocalID,2000);
			std::deque<Luna::Networking::Parsers::HTTP::Propertie_st> Tokens = Luna::Networking::Parsers::HTTP::Tokenize(Data);
			Luna::Networking::Parsers::HTTP::Header_st Header = Luna::Networking::Parsers::HTTP::Parse(Tokens);
			
			std::cout<<Header.ToString()<<std::endl;
			rSocketServer->Send(LocalID,Luna::Networking::Parsers::HTTP::ResponseHTTP(Data,200));
			rSocketServer->RemoveClient(LocalID);
		}
	};
};
