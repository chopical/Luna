#include "Utilities/Random.hpp"
#include <ctime>

namespace Luna{
    namespace Utilities{
        Random_cl::Random_cl(int64_t Seed,int64_t Max):m_Seed(Seed),m_Max(Max){
            if(!m_Seed){m_Seed=time(NULL);}
        }
        size_t Random_cl::Generate_Int(){
            m_Seed=(m_Seed*5+1)%m_Max;
            return m_Seed;
        }
        std::float64_t Random_cl::Generate_Float(){
            return static_cast<std::float64_t>(Generate_Int())/Max();
        }
        size_t Random_cl::Max(){return m_Max;}
        size_t Random_cl::Max(size_t nMax){m_Max=nMax;return Max();}
    };
};