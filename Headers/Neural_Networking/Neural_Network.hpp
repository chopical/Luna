#ifndef __LUNA_NEURAL_NETWORK__
#define __LUNA_NEURAL_NETWORK__
#include "Utilities/Random.hpp"
#include "Types.hpp"
#include <string>
#include <functional>
namespace Luna{
    namespace Neural_Networking{
        class Neural_Network_cl{
        Matrix_nt m_Values;
        Matrix_nt m_Weights;
        Matrix_nt m_DeltaWeights;
        Matrix_nt m_Biases;
        Luna::Utilities::Random_cl m_rand;
        size_nt m_Topology;
        std::function<std::float64_t(std::float64_t,std::float64_t)> m_Activate;
        std::function<std::float64_t(std::float64_t,std::float64_t)> m_Derive;
        public:
            Vector_nt GenerateRandom(size_t c);
            Neural_Network_cl(size_nt Topology,std::function<std::float64_t(std::float64_t,std::float64_t)>Activate,std::function<std::float64_t(std::float64_t,std::float64_t)>Derive,size_t seed=0);
            neuron_nt Get(size_t Y,size_t X);
            void FeedLoop(Vector_nt Input);
            void FeedForward(Vector_nt Input);
            void FeedBackward(Vector_nt Input);
            void BackProp(Vector_nt Input);
            Vector_nt Back();
            Vector_nt Front();
            std::string Str_Values();
            std::string Str_Weights();
            std::string Str_Biases();
            std::string Str();
        };
    };
};
#endif