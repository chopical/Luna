#include "Utilities/Arena_Allocator.hpp"

#include <cstring>
#include <cstdlib>


namespace Luna{
    namespace Utilities{
		Arena_Allocator_cl::Arena_Allocator_cl(size_t ArenaSize){
			this->ArenaSize=ArenaSize;
			Arena = (uint8_t*)malloc(ArenaSize);
			memset(Arena,0,ArenaSize);
		}
		size_t Arena_Allocator_cl::TotalUsed(){
			size_t Total=0;
			for(auto element : AllocatedSegments)Total+=element.size;
			return Total;
		}
		size_t Arena_Allocator_cl::TotalFree(){
			return ArenaSize-TotalUsed();
		}
		uint8_t* Arena_Allocator_cl::Seek(uint8_t* Addr){
			for(size_t i=0;i<AllocatedSegments.size();i++){
				if(AllocatedSegments.at(i).ptr == Addr){
					return AllocatedSegments.at(i).ptr;
				}
			}
			return nullptr;
		}
		uint8_t* Arena_Allocator_cl::Fit(size_t QueueSize){
			for(size_t i=1;i<AllocatedSegments.size();i++){
				if( (AllocatedSegments.at(i-1).ptr+AllocatedSegments.at(i-1).size) != AllocatedSegments.at(i).ptr ){
					size_t Room = ((size_t)(AllocatedSegments.at(i-1).ptr+AllocatedSegments.at(i-1).size))-((size_t)AllocatedSegments.at(i).ptr);
					if(Room >= QueueSize){
						SegmentData_st X{(AllocatedSegments.at(i-1).ptr+AllocatedSegments.at(i-1).size),QueueSize};
						AllocatedSegments.insert(AllocatedSegments.begin()+(i-1),X);
						return X.ptr;
					}
				}
			}
			if(TotalFree() > QueueSize){
				SegmentData_st X{Arena+TotalUsed(),QueueSize};
				AllocatedSegments.push_back(X);
				return X.ptr;
			}
			return nullptr;
		}
		uint8_t* Arena_Allocator_cl::BorrowRaw(size_t QueueSize,size_t Priority){
			uint8_t* Address = Fit(QueueSize);
			if(Address == nullptr)return nullptr;
			memset(Address,0,QueueSize);
			AllocatedSegments.push_back({Address,QueueSize,Priority});
			return Address;
		}
		void Arena_Allocator_cl::ReturnRaw(uint8_t* Addr){
			for(size_t i=0;i<AllocatedSegments.size();i++){
				if(AllocatedSegments.at(i).ptr == Addr){
					memset(AllocatedSegments.at(i).ptr,0,AllocatedSegments.at(i).size);
					AllocatedSegments.erase(AllocatedSegments.begin()+i);
					return;
				}
			}
		}
		void Arena_Allocator_cl::Yeet(size_t Priority){
			std::deque<uint8_t*> Yeetable;
			for(size_t i=0;i<AllocatedSegments.size();i++){
				if(AllocatedSegments.at(i).Priority == Priority)Yeetable.push_back(AllocatedSegments.at(i).ptr);
			}
			for(auto ptr : Yeetable){
				ReturnRaw(ptr);
			}
		}
	};
};