#ifndef __LUNA_UTILITIES_CHOAS__
#define __LUNA_UTILITIES_CHOAS__
#include "Utilities/Random.hpp"
#include <stdfloat>
#include <deque>
namespace Luna{
    namespace Utilities{
        template<typename T>
        struct Choas_st{
            struct Probability_st{
                std::float64_t Probability;
                T Value;
            };
            std::deque<Probability_st> Data;
            Luna::Utilities::Random_cl Rando;
            public:
            Choas_st():Rando(0,RAND_MAX){}
            Choas_st(std::deque<T> nV):Rando(0,RAND_MAX){Push(nV);}
            inline T Pull(std::float64_t target){
                std::float64_t counter=0.00;
                T result;
                for(size_t i=0;i<=Data.size();i++){
                    if(i==Data.size()){i=0;}
                    counter+=Data[i].Probability/Data.size();
                    if(counter>=target){result=Data[i].Value;break;}
                }
                return result;
            }
            inline T Pull(){
                std::float64_t target=Rando.Generate_Float();
                std::float64_t counter=0.00;
                T result;
                for(size_t i=0;i<=Data.size();i++){
                    if(i==Data.size()){i=0;}
                    counter+=Data[i].Probability/Data.size();
                    if(counter>=target){result=Data[i].Value;break;}
                }
                return result;
            }
            inline void Push(Probability_st nP){
                Data.push_back(nP);
            }
            inline void Push(std::float64_t nP,std::deque<T> nV){
                for(size_t i=0;i<nV.size();i++){
                    Push(Probability_st{nP,nV[i]});
                }
            }
            inline void Push(std::deque<T> nV){
                for(size_t i=0;i<nV.size();i++){
                    Push(Probability_st{1.0,nV[i]});
                }
            }
        };
    };
};
#endif