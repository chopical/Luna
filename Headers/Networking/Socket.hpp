#ifndef __LUNA_NETWORKING_SOCKET__
#define __LUNA_NETWORKING_SOCKET__
#include "Utilities/Random.hpp"

#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>
#include <deque>


namespace Luna{
	namespace Networking{
		struct Socket_st{
			struct Properties_st{
				int PortNumber{8080};
				int Domain{AF_INET};
				int Type{SOCK_STREAM};
				int Protocol{0};
				int Descriptor{-1};
				int Backlog{5};
				struct sockaddr_in Sockaddr;
			}Properties;
		};
		struct SocketClient_st : public Socket_st{
			SocketClient_st(std::string IP,int Port=80,int Domain=AF_INET,int Type=SOCK_STREAM,int Protocol=0);
			~SocketClient_st();
			void Send(std::string msg);
			std::string Receive(size_t bufferSize);
		};
		struct SocketServer_st : public Socket_st{
			struct Client_st{
				size_t LocalID{0};
				struct sockaddr_in Sockaddr;
				size_t SockaddrSize;
				int Descriptor;
			};
			Luna::Utilities::Random_cl m_rand;
			std::deque<Client_st> Clients;
			SocketServer_st(std::string IP="ANY",int Port=8080,int Domain=AF_INET,int Type=SOCK_STREAM,int Protocol=0);
			size_t Accept();
			size_t AddClient(Client_st& Client);
			void RemoveClient(size_t LocalID);
			std::string GetIP(size_t LocalID);
			void Send(size_t LocalID,std::string msg);
			std::string Receive(size_t LocalID,size_t bufferSize);
		};
	};
};
#endif
